#ifndef UNIVERSAL_LINE_SHADER_INCLUDED
#define UNIVERSAL_LINE_SHADER_INCLUDED
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "ShaderLibrary/UnityInput.hlsl"
struct v2f
{
    half4 pos : SV_POSITION;
};

CBUFFER_START(UnityPerMaterial)
    real4 _LineColor;
CBUFFER_END

v2f vert(appdata_base  v)
{
    v2f o;
    o.pos = TransformObjectToHClip(v.vertex.xyz);
    return o;
}

real4 frag(v2f i) : COLOR
{
    return _LineColor;
}
#endif