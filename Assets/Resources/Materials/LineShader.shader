﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LineColor" {

	Properties{
		_LineColor("Line Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader
	{
		Pass{
		Tags{ 
			"RenderPipeline" = "UniversalPipeline"
			"RenderType" = "Opaque"
		 }
		HLSLPROGRAM

		#pragma vertex vert
		#pragma fragment frag

		#include "LineShader.hlsl"
		ENDHLSL
		}
	}
}