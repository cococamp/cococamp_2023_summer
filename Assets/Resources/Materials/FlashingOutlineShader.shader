Shader "Unlit/FlashingOutlineShader"
{
     Properties 
    {
        _Outline ("Outline", Range(0, 1)) = 0.1
        _MainTex ("Texture", 2D) = "white" {}
        _OutlineColor ("Outline Color", Color) = (1, 0, 0, 1)
        _FlashSpeed ("Flash Speed", Range(1, 10)) = 5
        _EnableFlashing ("Enable Flashing", Int) = 1
        _BumpMap("Bump Map", 2D) = "black"{}
		_BumpScale ("Bump Scale", Range(0.1, 30.0)) = 10.0
        _Diffuse("Diffuse", Color) = (1,1,1,1)
	    _Metallic ("Metallic", Range(0, 1)) = 0
        _Glossiness ("Smoothness", Range(0, 1)) = 0.5
    }
    SubShader 
    {
        Pass 
        {
            Cull Back
               Tags {"LightMode"="ForwardBase"}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc" // 对于 _LightColor0
            float _Outline;
            fixed4 _OutlineColor;
            float _FlashSpeed;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            int _EnableFlashing;
            sampler2D _BumpMap;
			float4 _BumpMap_TexelSize;
			float _BumpScale;
			fixed4 _Diffuse;

            
            
            struct appdata {
                float4 vertex : POSITION;//获得顶点数据
                float2 uv : TEXCOORD0;//获得纹理坐标数据
                float4 normal: NORMAL;//获得法线数据
            };
            
            struct v2f
            {
                float4 pos : SV_POSITION;
                fixed4 color : COLOR;
                float2 uv : TEXCOORD0;//获得纹理坐标数
                float3 normal : TEXCOORD1;
            };

            v2f vert (appdata v)
            {       
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                float3 ObjViewDir = normalize(ObjSpaceViewDir(v.vertex));
                float3 normal = normalize(v.normal);
                float factor = step(_Outline, dot(normal, ObjViewDir));
                o.color = float4(1, 1, 1, 1) * factor;
                o.uv = v.uv;
                o.normal = normalize(UnityObjectToWorldNormal(v.normal));
                return o;
            }

            float4 frag(v2f i) : SV_Target 
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                float blink = abs(sin(_Time.y * _FlashSpeed));
                fixed4 outlineColor = _OutlineColor * (1 - i.color) * blink; // 根据闪烁效果计算描边颜色
                

                //unity自身的diffuse也是带了环境光，这里我们也增加一下环境光
				fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * _Diffuse.xyz;
				//归一化法线，即使在vert归一化也不行，从vert到frag阶段有差值处理，传入的法线方向并不是vertex shader直接传出的
				fixed3 worldNormal1 = normalize(i.normal);
				//采样bump贴图,需要知道该点的斜率，xy方向分别求，所以对于一个点需要采样四次
				fixed bumpValueU = tex2D(_BumpMap, i.uv + fixed2(-1.0 * _BumpMap_TexelSize.x, 0)).r - tex2D(_BumpMap, i.uv + fixed2(1.0 * _BumpMap_TexelSize.x, 0)).r;
				fixed bumpValueV = tex2D(_BumpMap, i.uv + fixed2(0, -1.0 * _BumpMap_TexelSize.y)).r - tex2D(_BumpMap, i.uv + fixed2(0, 1.0 * _BumpMap_TexelSize.y)).r;
				//用上面的斜率来修改法线的偏移值
				fixed3 worldNormal = fixed3(worldNormal1.x * bumpValueU * _BumpScale, worldNormal1.y * bumpValueV * _BumpScale, worldNormal1.z);
 
				//把光照方向归一化
				fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz);
				//根据半兰伯特模型计算像素的光照信息
				fixed3 lambert = 0.5 * dot(worldNormal, worldLightDir) + 0.5;
				//最终输出颜色为lambert光强*材质diffuse颜色*光颜色
				fixed3 diffuse = lambert * _Diffuse.xyz * _LightColor0.xyz + ambient;
            	fixed3 finialColor = diffuse * col.rgb;
                
                return  _EnableFlashing == 1 ? float4(finialColor.x,finialColor.y,finialColor.z,1) * i.color + outlineColor : float4(finialColor.x,finialColor.y,finialColor.z,1);
            }
            ENDCG
        }
    }
}
