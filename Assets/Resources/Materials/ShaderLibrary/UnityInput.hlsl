#ifndef UNIVERSAL_UNITY_INPUT_INCLUDED
#define UNIVERSAL_UNITY_INPUT_INCLUDED

struct appdata_base {
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float4 texcoord : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

#endif