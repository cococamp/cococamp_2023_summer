#ifndef UNIVERSAL_UNITY_SPRITES_INPUT_INCLUDED
#define UNIVERSAL_UNITY_SPRITES_INPUT_INCLUDED

struct appdata_t
{
    float4 vertex   : POSITION;
    float4 color    : COLOR;
    float2 texcoord : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct v2f
{
    float4 vertex   : SV_POSITION;
    real4 color    : COLOR;
    float2 texcoord : TEXCOORD0;
    UNITY_VERTEX_OUTPUT_STEREO
};

#endif