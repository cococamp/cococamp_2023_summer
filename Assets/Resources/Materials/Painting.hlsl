#ifndef UNIVERSAL_PAINTING_INCLUDED
#define UNIVERSAL_PAINTING_INCLUDED
#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
#include "ShaderLibrary/UnityInput.hlsl"

struct v2f
{
    float4 vertex : SV_POSITION;
    half2 texcoord : TEXCOORD0;
};

CBUFFER_START(UnityPerMaterial)
    real4 _Color;
CBUFFER_END

v2f vert(appdata_base IN)
{
    v2f OUT;
    OUT.vertex = TransformObjectToHClip(IN.vertex.xyz);
    OUT.texcoord = IN.texcoord.xy;
    return OUT;
}

sampler2D _MainTex;

real4 frag(v2f IN) : SV_Target
{
    float4 col = _Color * tex2D(_MainTex, IN.texcoord);
    col.rgb *= col.a;
    return col;
}
#endif