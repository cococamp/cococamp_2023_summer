using System;
using System.Collections;
using System.Collections.Generic;
using Logic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "OrganConfig", menuName = "CreateConfig/OrganConfig")]
    [Serializable]
    public class OrganConfig : ScriptableObject
    {
        [LabelText(@"器官总列表"), TableList]
        public List<OrganVO> organConfigList;
    }

    [Serializable]
    public class OrganVO
    {
        [VerticalGroup(@"器官类型"), HideLabel]
        public eOrganType organType;
        [VerticalGroup(@"器官名字"), HideLabel]
        public string name;
        [VerticalGroup(@"生命值"), HideLabel]
        public float HP;
        [VerticalGroup(@"初始生命值"), HideLabel]
        public float initHP;
        [VerticalGroup(@"阶段配置"), HideLabel, TableList]
        public List<OrganStateVO> organStateList;
    }

    [Serializable]
    public class OrganStateVO
    {
        [VerticalGroup(@"器官阶段"), HideLabel]
        public eOrganState state;
        [VerticalGroup(@"器官自己的生命值变化速度(秒)"), HideLabel]
        public float changeValue;
    }

    public enum eOrganType
    {
        [LabelText("心脏")]
        Heart,       //心脏
        [LabelText("胃")]
        Stomach,     // 胃
        [LabelText("肝脏")]
        Liver,      // 肝脏
        [LabelText("肾脏")]
        Kidney,     // 肾脏
        [LabelText("脾脏")]
        Spleen,     // 脾脏
        [LabelText("肺")]
        Lung,       // 肺
        [LabelText("肠")]
        Intestine,  // 肠
    }

}
