using System;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    // 形状配置
    [CreateAssetMenu(fileName = "ShapeConfig", menuName = "CreateConfig/ShapeConfig")]
    [Serializable]
    public class ShapeConfig : ScriptableObject
    {
        [LabelText(@"形状总列表"), TableList]
        public List<ShapeVO> shapeConfigList;
    }

    [Serializable]
    public class ShapeVO
    {
        [VerticalGroup(@"形状名字"), HideLabel]
        public string name;
        [VerticalGroup(@"prefab名字"), HideLabel]
        public string prefabName;
        [VerticalGroup(@"详细描述"), HideLabel, TextArea]
        public string desc;
        [VerticalGroup(@"形状资源名字"), HideLabel]
        [FilePath(ParentFolder = "Assets/Resources/Sprites/Shape", RequireExistingPath = true, Extensions = ".png")]
        [SerializeField]
        private string assetPath;
        public string assetName => Path.GetFileNameWithoutExtension(assetPath);
        [VerticalGroup(@"吸收速度(秒)"), HideLabel]
        public float baseSpeed; // 基础速度
        [VerticalGroup(@"药效曲线"), HideLabel]
        public AnimationCurve curve; // 药效曲线 
        [VerticalGroup(@"解锁等级"), HideLabel]
        public int unlockLevel;

        // 生命时间
        public float CurveLifeTime
        {
            get
            {
                if (curve != null && curve.length > 0)
                {
                    Keyframe[] keys = curve.keys;
                    return keys[keys.Length - 1].time;
                }

                return 0;
            }
        }
    }
}
