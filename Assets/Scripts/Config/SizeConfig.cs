using System;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    // 形状配置
    [CreateAssetMenu(fileName = "SizeConfig", menuName = "CreateConfig/SizeConfig")]
    [Serializable]
    public class SizeConfig : ScriptableObject
    {
        [LabelText(@"大小总列表"), TableList]
        public List<SizeVO> sizeConfigList;
    }

    [Serializable]
    public class SizeVO
    {
        [VerticalGroup(@"大小名字"), HideLabel]
        public string name;
        [VerticalGroup(@"药丸模型缩放大小"), HideLabel]
        public float pillObjectScale = 1.0f;
        [VerticalGroup(@"详细描述"), HideLabel, TextArea]
        public string desc;
        [VerticalGroup(@"形状资源名字"), HideLabel]
        [FilePath(ParentFolder = "Assets/Resources/Sprites/Size", RequireExistingPath = true, Extensions = ".png")]
        [SerializeField]
        private string assetPath;
        public string assetName => Path.GetFileNameWithoutExtension(assetPath);
        [VerticalGroup(@"格子大小"), HideLabel]
        public int cellSize;    // 格子大小
        [VerticalGroup(@"生命时长"), HideLabel]
        public int lifeTime;    // 格子大小
        [VerticalGroup(@"解锁等级"), HideLabel]
        public int unlockLevel;
    }
}
