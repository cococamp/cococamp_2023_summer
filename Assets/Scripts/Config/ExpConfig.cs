using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    // 等级配置
    [CreateAssetMenu(fileName = "ExpConfig", menuName = "CreateConfig/ExpConfig")]
    public class ExpConfig : ScriptableObject
    {
        [LabelText(@"等级经验配置"), TableList(ShowIndexLabels = true)]
        public List<ExpVO> expConfigList;
        [LabelText(@"恢复血量增加的经验系数")]
        public float addValue;
        [LabelText(@"减少血量增加的经验系数")]
        public float subValue;
    }

    [Serializable]
    public class ExpVO
    {
        [VerticalGroup(@"经验值"), HideLabel]
        public int expValue;
    }
}
