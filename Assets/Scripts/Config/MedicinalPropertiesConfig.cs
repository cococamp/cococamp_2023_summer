using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    // 药性配置
    [CreateAssetMenu(fileName = "MedicinalPropertiesConfig", menuName = "CreateConfig/MedicinalPropertiesConfig")]
    [Serializable]
    public class MedicinalPropertiesConfig : ScriptableObject
    {
        [LabelText(@"成分总列表"), TableList]
        public List<MedicinalProperties> medicinalPropertiesList;
    }

    [Serializable]
    public class MedicinalProperties
    {
        [VerticalGroup(@"名字"), HideLabel]
        public string name;
        [VerticalGroup(@"图标"), HideLabel]
        [FilePath(ParentFolder = "Assets/Resources/Sprites/Property", RequireExistingPath = true, Extensions = ".png")]
        [SerializeField]
        private string assetPath;
        public string assetName => Path.GetFileNameWithoutExtension(assetPath);
        [VerticalGroup(@"详细描述"), HideLabel, TextArea]
        public string desc;
        [VerticalGroup(@"解锁等级"), HideLabel]
        public int unlockLevel;
        [LabelText(@"对不同器官的效果"), TableList]
        public List<MedicinalPropertyEffect> effectList;
    }

    [Serializable]
    public class MedicinalPropertyEffect
    {
        [VerticalGroup(@"药效部位"), HideLabel]
        public eOrganType organType;
        [VerticalGroup(@"药效值 ±值决定效果"), HideLabel]
        public float effectValue;
    }
}

