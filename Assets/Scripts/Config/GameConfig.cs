using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Config
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "CreateConfig/GameConfig")]
    [Serializable]
    public class GameConfig : ScriptableObject
    {
        [LabelText("游戏时间(秒)")]
        public int gameTime;
    }
}

