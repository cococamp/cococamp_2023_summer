using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;
using System.IO;
using System.Linq;

namespace Config
{
    public class ConfigManager : SingletonMonoBehaviourClass<ConfigManager>
    {
        public List<ShapeVO> ShapeConfig => Get<ShapeConfig>().shapeConfigList;
        public List<SizeVO> SizeConfig => Get<SizeConfig>().sizeConfigList;
        public List<MedicinalProperties> PropertiesConfig => Get<MedicinalPropertiesConfig>().medicinalPropertiesList;
        public List<OrganVO> OrganList => Get<OrganConfig>().organConfigList;
        private readonly string ConfigFolderPath = "Configs";
        private string GetConfigPath(string subFolder, string configName)
        {
            return $"{ConfigFolderPath}/{subFolder}/{configName}";
        }

        private static Dictionary<Type, Dictionary<string, ScriptableObject>> _configDict = new Dictionary<Type, Dictionary<string, ScriptableObject>>();
        public T Get<T>(string configName = null) where T : ScriptableObject
        {
            var type = typeof(T);
            if (configName == null) configName = type.Name;
            if (_configDict.TryGetValue(type, out var dict))
            {
                if (dict.TryGetValue(configName, out var config)) return config as T;
            }
            else
            {
                dict = new Dictionary<string, ScriptableObject>();
                _configDict.Add(type, dict);
            }

            var newConfig = Resources.Load<T>(GetConfigPath(type.Name, configName));
            if (newConfig != null)
            {
                dict.Add(configName, newConfig);
                return newConfig;
            }
            else
            {
                Debug.LogError($"加载Config为空 type:{type.Name} fileName:{configName}");
                return null;
            }
        }
    }
}
