using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    // 配方配置
    [CreateAssetMenu(fileName = "FormulaConfig", menuName = "CreateConfig/FormulaConfig")]
    [Serializable]
    public class FormulaConfig : ScriptableObject
    {
        [TableList(ShowIndexLabels = true)]
        public List<FormulaConfigVO> formulaConfigList;
    }

    [Serializable]
    public class FormulaConfigVO
    {
        [VerticalGroup(@"配方名称"), HideLabel]
        public string name;
        [VerticalGroup(@"详细描述"), HideLabel, TextArea]
        public string desc;
        [VerticalGroup(@"增益数值"), HideLabel]
        public float formulaValue;
        [VerticalGroup(@"配方所需成分"), HideLabel]
        public List<string> propertiesList;
    }
}
