using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "CreateConfig/PlayerConfig")]
    [Serializable]
    public class PlayerConfig : ScriptableObject
    {
        [LabelText("加速度")]
        public float acc;

        [LabelText("最大速度")]
        public float maxVel;
    }
}

