using System;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPool
{
    public class ObjectPoolNode<T>
    {
        private object param;
        private Func<object, T> newFunc;
        private Func<T, T> instantiateFunc;

        private T m_prefab;
        private T prefab
        {
            get
            {
                if (m_prefab == null) m_prefab = newFunc(param);
                if (m_prefab == null) Debug.LogError($"prefab is null {(string)param}");
                return m_prefab;
            }
        }
        private Stack<T> instances = new Stack<T>();

        public ObjectPoolNode(object param, Func<object, T> newFunc, Func<T, T> instantiateFunc = null)
        {
            this.param = param;
            this.newFunc = newFunc;
            this.instantiateFunc = instantiateFunc;
        }

        public T Pop()
        {
            if (instances.Count > 0)
            {
                return instances.Pop();
            }
            else
            {
                if (instantiateFunc != null)
                {
                    return instantiateFunc(prefab);
                }
                else
                {
                    return newFunc(param);
                }
            }
        }

        public void Push(T obj)
        {
            instances.Push(obj);
        }
    }
}