using System;
using System.Collections.Generic;
using UnityEngine;

namespace ObjectPool
{
    public class ObjectPool<T>
    {
        private Func<object, T> newFunc;
        private Func<T, T> instantiateFunc;
        private Action<T> resetFunc;
        public Dictionary<object, ObjectPoolNode<T>> NodeDict = new Dictionary<object, ObjectPoolNode<T>>();
        public ObjectPool(Func<object, T> newFunc, Func<T, T> instantiateFunc = null, Action<T> resetFunc = null)
        {
            this.newFunc = newFunc;
            this.instantiateFunc = instantiateFunc;
            this.resetFunc = resetFunc;
        }

        public T Pop(object param)
        {
            if (!NodeDict.ContainsKey(param))
            {
                NodeDict.Add(param, new ObjectPoolNode<T>(param, newFunc, instantiateFunc));
            }
            return NodeDict[param].Pop();
        }

        public void Push(object param, T obj)
        {
            if (resetFunc != null)
            {
                resetFunc(obj);
            }
            if (!NodeDict.ContainsKey(param))
            {
                NodeDict.Add(param, new ObjectPoolNode<T>(param, newFunc, instantiateFunc));
            }
            NodeDict[param].Push(obj);
        }
    }
}