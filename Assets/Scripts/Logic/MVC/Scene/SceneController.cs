using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using Cameras;
using System;
using Utils;

namespace Logic
{
    public class SceneController : BaseController<SceneController>
    {
        public string currentSceneID { get; private set; }
        public string loadingSceneID { get; private set; }
        public string nextSceneID { get; private set; }
        private Action<bool> finishCallback;
        public bool IsLoaded => currentSceneID.IsFilled();
        public bool IsLoading => loadingSceneID.IsFilled();
        public bool HasNextScene => nextSceneID.IsFilled();
        public AsyncOperation op;
        public Action<bool> nextFinishCallback;
        protected override void OnInit()
        {

        }

        protected override void RegisterAllUI()
        {

        }

        public void LoadEmpty(Action<bool> callback)
        {
            LoadScene("Empty", callback);
        }

        public void LoadScene(string sceneID, Action<bool> callback)
        {
            if (HasNextScene)
            {
                nextFinishCallback?.Invoke(false);
                SceneController.instance.Log($"原本要加载的下一个场景:{nextSceneID} 被 {sceneID}覆盖了");
                nextSceneID = sceneID;
                nextFinishCallback = callback;
            }
            else
            {
                nextSceneID = sceneID;
                nextFinishCallback = callback;
            }
        }

        protected override void OnUpdate()
        {
            if (IsLoading)
            {
                if (op == null)
                {
                    loadingSceneID = null;
                    currentSceneID = null;
                    finishCallback?.Invoke(false);
                    finishCallback = null;
                }
                else if (op.isDone)
                {
                    currentSceneID = loadingSceneID;
                    loadingSceneID = null;
                    finishCallback?.Invoke(true);
                    finishCallback = null;
                }
            }
            else if (HasNextScene)
            {
                loadingSceneID = nextSceneID;
                finishCallback = nextFinishCallback;
                op = SceneManager.LoadSceneAsync(nextSceneID);
                nextSceneID = null;
                nextFinishCallback = null;
            }
        }
    }
}