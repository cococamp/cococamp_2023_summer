using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Logic
{
    public class ItemDescTipsUI : BaseMouseMoveUI
    {
        protected override UISort sort => UISort.ItemDescTipsUI;

        protected override string prefabName => "Tips/MouseTipsUI";
        protected override float offsetY => 20;
        protected override float offsetX => 20;
        protected override RectTransform bodyTransform => tipsTxt.transform.parent as RectTransform;

        private Text tipsTxt;
        private Sequence showSequence;

        protected override void OnInit()
        {
            tipsTxt = GetChild<Text>("bg/tips");
            showSequence = DOTween.Sequence()
            .AppendInterval(0.5f)
            .Append(sceneObject.GetComponent<CanvasGroup>().DOFade(1, 0.5f).From(0))
            .SetAutoKill(false);
            showSequence.Complete();
        }

        protected override void OnShow()
        {
            tipsTxt.text = (data as string);
            showSequence.Complete();
            showSequence.Restart();
        }

        protected override void OnHide()
        {
            showSequence.Complete();
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {

        }
    }
}