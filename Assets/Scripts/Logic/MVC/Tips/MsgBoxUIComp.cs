using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class MsgBoxUIComp : MonoBehaviour
    {
        public Text content;
        public GameObject btnOK;
        public GameObject btnCancel;
    }
}