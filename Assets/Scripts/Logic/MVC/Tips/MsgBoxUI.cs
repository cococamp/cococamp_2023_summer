using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;
using DG.Tweening;

namespace Logic
{
    public struct MsgBoxUIData
    {
        public string content;
        public bool isShowCancelBtn;
        public Action callbackOK;
        public Action callbackCancel;

    }
    public class MsgBoxUI : BaseUI<MsgBoxUIComp, MsgBoxUIData>
    {
        private Sequence showSequence;
        protected override UISort sort => UISort.MsgBoxUI;

        protected override string prefabName => "Tips/MsgBoxUI";

        protected override bool IsFullScreenUI => false;

        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "OK")
            {
                data.callbackOK?.Invoke();
                TipsController.instance.DestroyMsgBoxUI(this);
            }
            else if (obj.name == "Cancel")
            {
                data.callbackCancel?.Invoke();
                TipsController.instance.DestroyMsgBoxUI(this);
            }
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            if (showSequence != null)
            {
                showSequence.Kill();
                showSequence = null;
            }
        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            sceneComponent.content.text = data.content;
            sceneComponent.btnCancel.SetActiveOptimize(data.isShowCancelBtn);
            showSequence = DOTween.Sequence()
                .Append(sceneObject.transform.DOScale(1, 1.0f).From(0.5f).SetEase(Ease.OutBack))
                .Join(sceneObject.transform.GetComponent<CanvasGroup>().DOFade(1, 1.0f).From(0));
            AudioManager.instance.PlayEffect("UI");
        }
    }
}