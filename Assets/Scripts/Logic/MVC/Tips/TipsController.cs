using System.Collections.Generic;
using Config;
using UnityEngine;
using System;

namespace Logic
{
    public class TipsController : BaseController<TipsController>
    {
        protected override void OnInit()
        {

        }
        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.MsgBoxUI, new MsgBoxUI());
            RegisterUI(UISort.ItemDescTipsUI, new ItemDescTipsUI());
        }
        public void ShowTips(string tip)
        {
            var tipsUI = new TipsUI();
            tipsUI.Init();
            tipsUI.Show(tip);
        }

        private List<MsgBoxUI> msgBoxUIList = new List<MsgBoxUI>();
        public void ShowMsgBox(MsgBoxUIData data)
        {
            var newMsgBoxUI = new MsgBoxUI();
            newMsgBoxUI.Init();
            newMsgBoxUI.Show(data);
            msgBoxUIList.Add(newMsgBoxUI);
        }

        public void DestroyMsgBoxUI(MsgBoxUI msgBoxUI)
        {
            msgBoxUIList.Remove(msgBoxUI);
            msgBoxUI.Destroy();
        }

        public void DestroyAllMsgBoxUI()
        {
            foreach (var msgBoxUI in msgBoxUIList)
            {
                msgBoxUI.Destroy();
            }
            msgBoxUIList.Clear();
        }
    }
}