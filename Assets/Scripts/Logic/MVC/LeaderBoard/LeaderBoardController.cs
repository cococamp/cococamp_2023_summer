using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

namespace Logic
{
    public class LeaderBoardController : BaseController<LeaderBoardController, LeaderBoardModel>
    {
        private LeaderBoardService leaderBoardService;

        protected override void OnInit()
        {
            
        }
        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.LeaderBoardUI, new LeaderBoardUI());
            RegisterUI(UISort.InputNameUI, new InputNameUI());
            RegisterUI(UISort.SubmitUI, new SubmitUI());
        }

        protected override void OnFixedUpdate()
        {
            base.OnFixedUpdate();
            Model.FixedUpdate();
        }
    }
}