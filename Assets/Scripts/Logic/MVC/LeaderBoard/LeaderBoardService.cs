using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Logic
{
    public class LeaderBoardService : SingletonMonoBehaviourClass<LeaderBoardService>
    {
        private const string MyWebSite = "http://www.dreamlo.com/lb/4ske7Pp1_Ey_AoNJvtQNXgRLWDf86L-U2CYmFxULlEVQ";
        public const string rootURL = "http://dreamlo.com/lb/";
        public const string privateKey = "4ske7Pp1_Ey_AoNJvtQNXgRLWDf86L-U2CYmFxULlEVQ";
        public const string publicKey = "63940066778d3c1f102b66ed";
        public void Init()
        {

        }

        public void Destroy()
        {

        }

        public void AddScore(string name, int score, Action<bool> callback)
        {
            var url = $"{rootURL}{privateKey}/add/{SystemInfo.deviceUniqueIdentifier}/{score}/0/{UnityWebRequest.EscapeURL(name)}";
            RequestWeb(url, (success, txt) =>
            {
                callback?.Invoke(success);
            });
        }

        public void GetAllScore(Action<bool, List<UserData>> callback)
        {
            var url = $"{rootURL}{publicKey}/json";
            RequestWeb(url, (success, data) =>
            {
                List<UserData> userDatas = null;
                if (success && data != null)
                {
                    var Jdata = JObject.Parse(data);
                    var leaderboard = Jdata["dreamlo"]["leaderboard"];
                    if (leaderboard is JObject)
                    {
                        var entry = leaderboard["entry"];
                        if (entry is JArray)
                        {
                            userDatas = entry.ToObject<List<UserData>>();
                        }
                        else if (entry is JObject)
                        {
                            userDatas = new List<UserData>() { Jdata["dreamlo"]["leaderboard"]["entry"].ToObject<UserData>() };
                        }
                        if (userDatas.Count > 25)
                        {
                            userDatas.RemoveRange(25, userDatas.Count - 25);
                        }
                    }
                }
                //var jsonData = JsonConvert.DeserializeObject(data).
                callback?.Invoke(success, userDatas);
            });
        }

        private void RequestWeb(string url, Action<bool, string> callback)
        {
            if (isRequesting)
            {
                callback(false, null);
                return;
            }
            StartCoroutine(I_RequestWeb(url, callback));
        }

        private bool isRequesting = false;
        IEnumerator I_RequestWeb(string url, Action<bool, string> callback)
        {
            isRequesting = true;
            using (UnityWebRequest request = UnityWebRequest.Get(url))
            {
                Debug.Log($">>> {url}");
                yield return request.SendWebRequest();
                if (request.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"<<< {url} result:{request.result} error:{request.error}");
                    isRequesting = false;
                    callback(false, null);
                }
                else
                {
                    var data = request.downloadHandler?.text;
                    Debug.Log($"<<< {url} result:{request.result} data:{data}");
                    isRequesting = false;
                    if (data.StartsWith("ERROR"))
                    {
                        callback(false, null);
                    }
                    else
                    {
                        callback(true, data);
                    }
                }
            }
        }
    }

    public class UserData
    {
        public string name;
        public int score;
        public int seconds;
        public string text;
        public DateTime date;

        public bool IsMyData(string name, int score)
        {
            return this.name == SystemInfo.deviceUniqueIdentifier && this.score == score && this.text == name;
        }
    }
}