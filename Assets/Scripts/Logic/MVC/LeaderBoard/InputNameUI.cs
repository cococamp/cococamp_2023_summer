using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class InputNameUI : BaseUI<InputNameUIComp, int>
    {
        protected override string prefabName => "LeaderBoard/InputNameUI";

        protected override UISort sort => UISort.InputNameUI;
        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            sceneComponent.scoreText.text = $"得分：{data}";
            RefreshName();
        }

        private void RefreshName()
        {
            bool haveName = LeaderBoardController.instance.Model.GetName(out string name);
            if (haveName)
            {
                Hide();
                LeaderBoardController.instance.ShowUI(UISort.SubmitUI, data);
            }
            else
            {
                sceneComponent.inputField.text = "";
                sceneComponent.btnOK.interactable = false;
            }
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
        protected override void RegisterEvents()
        {
            sceneComponent.inputField.onValueChanged.AddListener(OnInputValueChanged);
            EventManager.Get<EventDefine.NameRefreshEvent>().AddListener(RefreshName);
        }
        protected override void UnRegisterEvents()
        {
            sceneComponent.inputField.onValueChanged.RemoveListener(OnInputValueChanged);
            EventManager.Get<EventDefine.NameRefreshEvent>().RemoveListener(RefreshName);
        }

        protected override void OnUpdate()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "btnOK")
            {
                var name = sceneComponent.inputField.text;
                if (name.Contains("*"))
                {
                    TipsController.instance.ShowTips("名字里不能包含 “*”");
                    return;
                }

                //LeaderBoardController.instance.Model.SaveName(name);
                TipsController.instance.ShowMsgBox(new MsgBoxUIData()
                {
                    content = $"确定使用<color=#FF3400>{name}</color>作为您的名字吗？确定后将无法更改！",
                    isShowCancelBtn = true,
                    callbackOK = () =>
                    {
                        LeaderBoardController.instance.Model.SaveName(name);
                    }
                });
            }
            // else if (obj.name == "btnSubmit")
            // {
            //     LeaderBoardController.instance.Model.SaveHighScore(data);
            // }
            // else if (obj.name == "btnClose")
            // {
            //     Hide();
            //     LeaderBoardController.instance.ShowUI(UISort.LeaderBoardUI);
            // }
        }

        private void OnInputValueChanged(string inputName)
        {
            sceneComponent.btnOK.interactable = !inputName.IsNullOrEmpty();
        }
    }
}