using UnityEngine;
using UnityEngine.UI;
namespace Logic
{
    public class SubmitUIComp : MonoBehaviour
    {
        public Text scoreText;
        public Text nameText;
        public Button btnSubmit;
    }
}