using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Logic
{
    public class DebugController : BaseController<DebugController, DebugModel>
    {
        protected override void OnInit()
        {

        }
        protected override void OnFixedUpdate()
        {
#if UNITY_EDITOR
            if (Keyboard.current.f1Key.wasPressedThisFrame)
            {

            }

            if (Keyboard.current.f2Key.wasPressedThisFrame)
            {

            }

            UpdateDebugUI();
#endif
        }

#if UNITY_EDITOR
        private HashSet<DebugUI> debugUIList = new HashSet<DebugUI>();
#endif
        public DebugUI CreateDebugUI(Func<string> logGetter, Vector2 alignment)
        {
#if UNITY_EDITOR
            var debugUI = new DebugUI();
            debugUI.Init(logGetter, alignment);
            debugUI.Show();
            debugUIList.Add(debugUI);
            return debugUI;
#else
            return null;
#endif

        }

        public void DestroyDebugUI(DebugUI debugUI)
        {
#if UNITY_EDITOR
            debugUIList.Remove(debugUI);
            debugUI.Destroy();
#endif
        }

#if UNITY_EDITOR
        private void UpdateDebugUI()
        {
            foreach (var debugUI in debugUIList)
            {
                debugUI.Update();
            }
        }
#endif
    }
}