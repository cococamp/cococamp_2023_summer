using System;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class DebugUI : BaseUI
    {
        protected override UISort sort => UISort.DebugUI;

        protected override string prefabName => "Debug/DebugUI";
        private Func<string> logGetter;
        private Vector2 alignment;


        public void Init(Func<string> logGetter, Vector2 alignment)
        {
            this.logGetter = logGetter;
            this.alignment = alignment;
            base.Init();
        }

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            var rectTransform = sceneObject.GetComponent<RectTransform>();
            rectTransform.pivot = alignment;
            rectTransform.anchorMin = alignment;
            rectTransform.anchorMax = alignment;
        }

        protected override void OnUpdate()
        {
            Refresh();
        }

        private void Refresh()
        {
            sceneObject.GetChild<Text>("controlState").text = logGetter?.Invoke();
        }
    }
}