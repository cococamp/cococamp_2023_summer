namespace Logic
{
    //序号越高  在UI里的层级越高
    public enum UISort
    {
        IntroUI,
        PrescriptionUI,
        GameFinishUI,
        //MainUI,
        #region LeaderBoard
        LeaderBoardUI,
        SubmitUI,
        InputNameUI,
        #endregion
        GameUI,

        #region Tips
        ItemDescTipsUI,
        MsgBoxUI,
        TipsUI,
        #endregion

        #region Debug
        DebugUI,

        #endregion
    }
}