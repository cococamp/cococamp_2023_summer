namespace Logic
{
    public interface IModel
    {
        void Init();
        void Destroy();
    }
    public abstract class BaseModel
    {
        public abstract void Init();
        public abstract void Destroy();
    }
}