using System.Collections.Generic;

namespace Logic
{
    public class ModuleManager : SingletonClass<ModuleManager>
    {
        private void RegisterAllModules()
        {
            RegisterModule(GameController.instance);
            RegisterModule(DebugController.instance);
            RegisterModule(TipsController.instance);
            RegisterModule(LeaderBoardController.instance);
            RegisterModule(SceneController.instance);
            RegisterModule(OrganController.instance);
            RegisterModule(PillController.instance);
            RegisterModule(ExpController.instance);
        }

        private HashSet<IController> moduleList;
        private void RegisterModule(IController controller)
        {
            controller.Init();
            moduleList.Add(controller);
        }

        public void InitAllModules()
        {
            moduleList = new HashSet<IController>();
            RegisterAllModules();
            EventManager.Get<EventDefine.ModulesInitFinished>().Fire();
        }

        public void Update()
        {
            if (GameController.instance.PauseGame)
            {
                DebugController.instance.Update();
            }
            else
            {
                foreach (var controller in moduleList)
                {
                    controller.Update();
                }
            }
        }

        public void FixedUpdate()
        {
            if (GameController.instance.PauseGame)
            {
                DebugController.instance.FixedUpdate();
            }
            else
            {
                foreach (var controller in moduleList)
                {
                    controller.FixedUpdate();
                }
            }
        }

        public void DestroyAllModules()
        {
            foreach (var controller in moduleList)
            {
                controller.Destroy();
            }
        }
    }
}