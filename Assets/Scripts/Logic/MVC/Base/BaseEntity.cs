using UnityEngine;
using ObjectPool;
using System.Collections.Generic;

namespace Logic
{
    public abstract class BaseEntity<T, V> : BaseEntity<T> where T : MonoBehaviour
    {
        protected new V data => (V)base.data;
    }

    public abstract class BaseEntity<T> : BaseEntity where T : MonoBehaviour
    {
        public T sceneComponent;
        protected override void InitObject()
        {
            base.InitObject();
            sceneComponent = sceneObject.GetComponent<T>();
        }
    }

    public abstract class BaseEntity : BaseView
    {
        protected virtual Vector3 initPos => Vector3.zero;
        private bool initWithGameObject = false;
        private string m_prefabName;
        protected override string prefabName => m_prefabName;

        /// <summary>
        /// 通过场景中已有的GameObject来初始化
        /// </summary>
        /// <param name="obj"></param>
        public void InitWithGameObject(GameObject obj)
        {
            if (obj == null) return;
            initWithGameObject = true;
            if (isInit) return;
            //将场景中的Obj直接赋值给sceneObject;
            sceneObject = obj;
            InitObject();
            RegisterClicks();
            RegisterEvents();
            RegisterAllTriggerContainer();
            RegisterColliders();
            OnInit();

            //如果已经显示了  直接调用OnShow()
            if (isShow)
            {
                OnShow();
            }
        }

        /// <summary>
        /// 通过传入的prefabName来初始化
        /// </summary>
        /// <param name="prefabName"></param>
        public void Init(string prefabName)
        {
            m_prefabName = prefabName;
            Init();
        }

        /// <summary>
        /// 通过prefabName来初始化
        /// </summary>
        public override void Init()
        {
            initWithGameObject = false;
            if (isInit) return;
            InitObject();
            RegisterClicks();
            RegisterEvents();
            RegisterAllTriggerContainer();
            RegisterColliders();
            OnInit();
        }

        public override void Destroy()
        {
            if (!isInit) return;
            if (isShow) Hide();
            OnDestroy();
            if (sceneObject != null)
            {
                UnRegisterClicks();
                UnRegisterEvents();
                UnRegisterColliders();
                UnRegisterAllTriggerContainers();
                DestroySceneObject();
            }
        }

        protected sealed override void RegisterClicks()
        {
            var collider2Ds = sceneObject.transform.GetComponentsInChildren<Collider2D>(true);
            foreach (var collider2D in collider2Ds)
            {
                var eventTriggerListener = EventTriggerListener.Get(collider2D.gameObject);
                eventTriggerListener.onClick += OnClick;
                eventTriggerListener.onEnter += OnEnter;
                eventTriggerListener.onExit += OnExit;
            }
        }

        protected sealed override void UnRegisterClicks()
        {
            var collider2Ds = sceneObject.transform.GetComponentsInChildren<Collider2D>(true);
            foreach (var collider2D in collider2Ds)
            {
                var eventTriggerListener = EventTriggerListener.Get(collider2D.gameObject);
                eventTriggerListener.onClick -= OnClick;
                eventTriggerListener.onEnter -= OnEnter;
                eventTriggerListener.onExit -= OnExit;
            }
        }

        private bool isCollidersRegistered = false;
        private void RegisterColliders()
        {
            foreach (var collider2D in sceneObject.GetComponentsInChildren<Collider2D>())
            {
                var handler = ColliderHandler.Get(collider2D.gameObject);
                handler.root = sceneObject;
                handler.onTriggerEnter += OnTriggerEnter2D;
                handler.onTriggerExit += OnTriggerExit2D;
                handler.onTriggerStay += OnTriggerStay2D;

                if (triggerContainerList != null && triggerContainerList.Count > 0)
                {
                    foreach (var triggerContainer in triggerContainerList)
                    {
                        handler.onTriggerEnter += triggerContainer.OnTriggerEnter2D;
                        handler.onTriggerExit += triggerContainer.OnTriggerExit2D;
                    }
                }
            }
            isCollidersRegistered = true;
        }

        private void UnRegisterColliders()
        {
            foreach (var collider2D in sceneObject.GetComponentsInChildren<Collider2D>())
            {
                var handler = ColliderHandler.Get(collider2D.gameObject);
                handler.root = null;
                handler.onTriggerEnter -= OnTriggerEnter2D;
                handler.onTriggerExit -= OnTriggerExit2D;
                handler.onTriggerStay -= OnTriggerStay2D;

                if (triggerContainerList != null && triggerContainerList.Count > 0)
                {
                    foreach (var triggerContainer in triggerContainerList)
                    {
                        handler.onTriggerEnter -= triggerContainer.OnTriggerEnter2D;
                        handler.onTriggerExit -= triggerContainer.OnTriggerExit2D;
                    }
                }
            }
            isCollidersRegistered = false;
        }

        protected sealed override void CreateSceneObject()
        {
            if (!initWithGameObject)
            {
                sceneObject = ObjectPoolManager.instance.ObjectPool.Pop(prefabName);
                ViewManager.instance.AddObject(sceneObject, initPos);
            }
            if (sceneObject.TryGetComponent<ITriggerableComponent>(out var comp))
            {
                comp.entity = this;
            }
        }

        protected sealed override void DestroySceneObject()
        {
            if (sceneObject.TryGetComponent<ITriggerableComponent>(out var comp))
            {
                comp.entity = null;
            }
            if (!initWithGameObject)
            {
                ObjectPoolManager.instance.ObjectPool.Push(prefabName, sceneObject);
            }
            sceneObject = null;
        }

        private HashSet<ITriggerContainer> triggerContainerList;
        protected virtual void RegisterAllTriggerContainer()
        {

        }
        protected TriggerContainer<TComp, TEntity> RegisterTriggerContainer<TComp, TEntity>() where TComp : ITriggerableComponent where TEntity : BaseEntity
        {
            if (isCollidersRegistered)
            {
                Debug.LogError("请在RegisterAllTriggerContainer中注册TriggerContainer");
                return null;
            }
            triggerContainerList ??= new HashSet<ITriggerContainer>();
            var newContainer = new TriggerContainer<TComp, TEntity>();
            triggerContainerList.Add(newContainer);
            return newContainer;
        }

        private void UnRegisterAllTriggerContainers()
        {
            triggerContainerList = null;
        }

        protected virtual void OnEnter(GameObject obj)
        {

        }

        protected virtual void OnExit(GameObject obj)
        {

        }

        protected virtual void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {

        }

        protected virtual void OnTriggerStay2D(GameObject obj, Collider2D collider2D)
        {

        }

        protected virtual void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {

        }
    }
}