using System.Collections.Generic;
using UnityEngine;

namespace Logic
{
    public interface ITriggerContainer
    {
        void OnTriggerEnter2D(GameObject obj, Collider2D collider2D);
        void OnTriggerExit2D(GameObject obj, Collider2D collider2D);
    }
    public class TriggerContainer<TComp, TEntity>:ITriggerContainer where TComp : ITriggerableComponent where TEntity : BaseEntity
    {
        public List<TEntity> triggerEntities { get; private set; } = new List<TEntity>();
        public TEntity LastEnterEntity => triggerEntities.Count > 0 ? triggerEntities[triggerEntities.Count - 1] : null;
        public TEntity FirstEnterEntity => triggerEntities.Count > 0 ? triggerEntities[0] : null;
        public int Count => triggerEntities.Count;

        public void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {
            var optionEntityComp = collider2D.GetRootGO<ITriggerableComponent>();
            if (optionEntityComp != null && optionEntityComp.entity != null)
            {
                triggerEntities.Add(optionEntityComp.entity as TEntity);
            }
        }

        public void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {
            var optionEntityComp = collider2D.GetRootGO<ITriggerableComponent>();
            if (optionEntityComp != null && optionEntityComp.entity != null)
            {
                triggerEntities.Remove(optionEntityComp.entity as TEntity);
            }
        }
    }

    public interface ITriggerableComponent
    {
        BaseEntity entity { get; set; }
    }
}