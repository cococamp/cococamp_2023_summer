using UnityEngine;
using ObjectPool;
using UnityEngine.UI;

namespace Logic
{
    public abstract class BaseUI<T, V> : BaseUI<T> where T : MonoBehaviour
    {
        protected new V data => (V)base.data;
    }
    public abstract class BaseUI<T> : BaseUI where T : MonoBehaviour
    {
        protected T sceneComponent;
        protected override void InitObject()
        {
            base.InitObject();
            sceneComponent = sceneObject.GetComponent<T>();
        }
    }

    public abstract class BaseUI : BaseView
    {
        protected abstract UISort sort { get; }
        protected RectTransform rectTransform => sceneObject.transform as RectTransform;
        protected virtual bool IsFullScreenUI => true;

        protected sealed override void CreateSceneObject()
        {
            sceneObject = ObjectPoolManager.instance.UIPool.Pop(prefabName);
            ViewManager.instance.AddUI(sceneObject, sort);
            if (IsFullScreenUI)
            {
                RectTransform rect = sceneObject.transform as RectTransform;
                rect.anchorMin = new Vector2(0, 0);
                rect.anchorMax = new Vector2(1, 1);
                rect.sizeDelta = Vector2.zero;
            }
        }

        protected sealed override void DestroySceneObject()
        {
            ObjectPoolManager.instance.UIPool.Push(prefabName, sceneObject);
            sceneObject = null;
        }

        protected sealed override void RegisterClicks()
        {
            Button[] buttons = sceneObject.GetComponentsInChildren<Button>(true);
            foreach (var b in buttons)
            {
                var clickHandler = ClickHandler.Get(b.gameObject);
                clickHandler.onClick += OnClick;
            }
        }
        protected sealed override void UnRegisterClicks()
        {
            Button[] buttons = sceneObject.GetComponentsInChildren<Button>(true);
            foreach (var b in buttons)
            {
                var clickHandler = ClickHandler.Get(b.gameObject);
                clickHandler.onClick -= OnClick;
            }
        }

        //清空  然后重新设置一下
        protected void ResetAllClicks()
        {
            Button[] buttons = sceneObject.GetComponentsInChildren<Button>(true);
            foreach (var b in buttons)
            {
                var clickHandler = ClickHandler.Get(b.gameObject);
                clickHandler.onClick = null;
                clickHandler.onClick += OnClick;
            }
        }
    }
}