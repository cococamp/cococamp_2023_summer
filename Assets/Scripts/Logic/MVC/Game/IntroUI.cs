using Config;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Logic
{
    public class IntroUI : BaseUI
    {
        protected override UISort sort => UISort.IntroUI;

        protected override string prefabName => "Game/IntroUI";
        private MultiItemManager levelBtnList;
        private LevelConfigItem[] levelList;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnStart":
                    GameController.instance.LoadLevel("Level1");
                    //sceneObject.GetChild("bg/btnStart").SetActiveOptimize(false);
                    //levelBtnList.gameObject.SetActiveOptimize(true);
                    break;
            }
            if (obj.name.StartsWith("Button"))
            {
                GameController.instance.LoadLevel(obj.GetChild<Text>("Text").text);
            }
        }

        protected override void OnInit()
        {
            levelList = ConfigManager.instance.Get<LevelConfig>().levelList;
            levelBtnList = sceneObject.GetChild<MultiItemManager>("bg/levelList");
            levelBtnList.SetSize(levelList.Length);
            for (int i = 0; i < levelList.Length; i++)
            {
                var btnTransform = levelBtnList.GetItem<Transform>(i);
                btnTransform.Find("Text").GetComponent<Text>().text = levelList[i].SceneID;
            }
            ResetAllClicks();
        }

        protected override void OnShow()
        {
            sceneObject.GetChild("bg/btnStart").SetActiveOptimize(true);
            levelBtnList.gameObject.SetActiveOptimize(false);
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
    }
}