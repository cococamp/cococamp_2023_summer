using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Logic
{
    public class ExpController : BaseController<ExpController, ExpModel>
    {
        public void AddExp(float expValue)
        {
            expValue = Mathf.Abs(expValue > 0 ? expValue * Model.addMultiValue : expValue * Model.subMultiValue);
            Model.AddExp(expValue);
        }
    }
}

