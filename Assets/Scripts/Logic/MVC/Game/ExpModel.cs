using System.Collections;
using System.Collections.Generic;
using Config;
using UnityEngine;
using System.Linq;

namespace Logic
{
    public class ExpModel : BaseModel
    {
        private int playerLevel;
        public int PlayerLevel => playerLevel;
        private float currentExp;
        public float CurrentExp => currentExp;
        private float nextLevelExp;
        public float NextLevelExp => nextLevelExp;

        public float addMultiValue;
        public float subMultiValue;

        public override void Init()
        {

        }

        public void InitData()
        {
            var config = ConfigManager.instance.Get<ExpConfig>();

            playerLevel = 0;
            currentExp = 0;
            nextLevelExp = config.expConfigList[0].expValue;

            addMultiValue = config.addValue;
            subMultiValue = config.subValue;
        }

        public override void Destroy()
        {

        }

        public void AddExp(float expValue)
        {
            currentExp += expValue;
            if (currentExp >= nextLevelExp && playerLevel < ConfigManager.instance.Get<ExpConfig>().expConfigList.Count - 1)
            {
                playerLevel += 1;
                currentExp -= nextLevelExp;
                var config = ConfigManager.instance.Get<ExpConfig>().expConfigList;
                nextLevelExp = config.Count <= playerLevel ? int.MaxValue : config[playerLevel].expValue;
                ShowNewLevelItems(playerLevel);
                EventManager.Get<EventDefine.LevelUpgrade>().Fire(playerLevel);
            }
        }

        private void ShowNewLevelItems(int newLevel)
        {
            string content = "恭喜解锁了新的东西";
            var newShapes = ConfigManager.instance.ShapeConfig.Where(config => config.unlockLevel == newLevel).Select(config => config.name);
            if (newShapes.Any())
            {
                content += "\n形状：";
                content += string.Join("、", newShapes);
            }

            var newSize = ConfigManager.instance.SizeConfig.Where(config => config.unlockLevel == newLevel).Select(config => config.name);
            if (newSize.Any())
            {
                content += "\n大小：";
                content += string.Join("、", newSize);
            }

            var newProperties = ConfigManager.instance.PropertiesConfig.Where(config => config.unlockLevel == newLevel).Select(config => config.name);
            if (newProperties.Any())
            {
                content += "\n成分：";
                content += string.Join("、", newProperties);
            }
            TipsController.instance.ShowMsgBox(new MsgBoxUIData()
            {
                content = content,
                isShowCancelBtn = false,
                callbackOK = () =>
                {

                },
            });
        }
    }
}

