using UnityEngine;
using Config;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

namespace Logic
{
    public class PrescriptionUI : BaseUI<PrescriptionUIComp>
    {
        protected override UISort sort => UISort.PrescriptionUI;

        protected override string prefabName => "Game/PrescriptionUI";
        private int currentSelectShapeIndex = -1;
        private int currentSelectSizeIndex = -1;
        protected override bool IsFullScreenUI => false;
        private ShapeVO CurrentSelectShapeVO
        {
            get
            {
                if (currentSelectShapeIndex >= 0)
                {
                    var configs = unlockedShapeConfigs;
                    if (currentSelectShapeIndex < configs.Count)
                    {
                        return configs[currentSelectShapeIndex];
                    }
                }
                return null;
            }
        }

        private SizeVO CurrentSelectSizeVO
        {
            get
            {
                if (currentSelectSizeIndex >= 0)
                {
                    var configs = unlockedSizeConfigs;
                    if (currentSelectSizeIndex < configs.Count)
                    {
                        return configs[currentSelectSizeIndex];
                    }
                }
                return null;
            }
        }

        private List<MedicinalProperties> currentSelectProperties = new List<MedicinalProperties>();

        private List<ShapeVO> unlockedShapeConfigs;
        private List<SizeVO> unlockedSizeConfigs;
        private List<MedicinalProperties> unlockedPropertyConfigs;

        protected override void OnInit()
        {
            sceneComponent.drawLineBehavior.OnDrawDone = OnDrawDown;
            sceneComponent.drawLineBehavior.CanDraw = CanDraw;
            sceneComponent.drawLineBehavior.OnDrawBegin = () =>
            {
                sceneComponent.signSound.Play();
            };
            sceneComponent.drawLineBehavior.OnUp = () =>
            {
                sceneComponent.signSound.Stop();
            };
        }



        private void RefreshUnlockItems(bool isReset)
        {
            if (isReset)
            {
                unlockedShapeConfigs = new List<ShapeVO>();
                unlockedPropertyConfigs = new List<MedicinalProperties>();
                unlockedSizeConfigs = new List<SizeVO>();
            }

            var currentLevel = ExpController.instance.Model.PlayerLevel;

            foreach (var item in ConfigManager.instance.ShapeConfig)
            {
                if (currentLevel >= item.unlockLevel && !unlockedShapeConfigs.Contains(item))
                {
                    unlockedShapeConfigs.Add(item);
                }
            }

            foreach (var item in ConfigManager.instance.SizeConfig)
            {
                if (currentLevel >= item.unlockLevel && !unlockedSizeConfigs.Contains(item))
                {
                    unlockedSizeConfigs.Add(item);
                }
            }

            foreach (var item in ConfigManager.instance.PropertiesConfig)
            {
                if (currentLevel >= item.unlockLevel && !unlockedPropertyConfigs.Contains(item))
                {
                    unlockedPropertyConfigs.Add(item);
                }
            }
        }

        public Sequence showSequence;

        protected override void OnShow()
        {
            RefreshUnlockItems(true);
            ResetViewData();
            RefreshShapes();
            RefreshSize();
            RefreshProperties();
            RefreshDocks();
            RefreshButton();

            Cameras.CameraManager.instance.sceneCamera.GetComponent<Animator>().Play("far");
            showSequence = DOTween.Sequence()
            .Append(rectTransform.DOAnchorPosY(0, 1.0f).From(new Vector2(0, 815)).SetEase(Ease.OutBack));

            AudioManager.instance.PlayEffect("UI");

            sceneComponent.signSound.Stop();
        }

        private void ResetViewData()
        {
            currentSelectShapeIndex = -1;
            currentSelectSizeIndex = -1;
            currentSelectProperties.Clear();
        }

        protected override void OnHide()
        {
            if (showSequence != null)
            {
                showSequence.Kill();
                showSequence = null;
            }

            sceneComponent.signSound.Stop();
        }
        protected override void OnDestroy()
        {
            sceneComponent.drawLineBehavior.OnDrawDone = null;
            sceneComponent.drawLineBehavior.CanDraw = null;
            if (fadeTween != null)
            {
                fadeTween.Kill();
                fadeTween = null;
            }
        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.LevelUpgrade>().AddListener(OnLevelUpgrade);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.LevelUpgrade>().RemoveListener(OnLevelUpgrade);
        }
        protected override void OnClick(GameObject obj)
        {
            if (obj.name.StartsWith("SizeItem"))
            {
                var index = sceneComponent.SizeGroup.GetItemIndex(obj);
                OnSizeItemClicked(index);
            }
            else if (obj.name.StartsWith("ShapeItem"))
            {
                var index = sceneComponent.shapeGroup.GetItemIndex(obj);
                OnShapeItemClicked(index);
            }
            else if (obj.name.StartsWith("BtnProperty"))
            {
                var index = sceneComponent.PropertyGroup.GetItemIndex(obj.transform.parent.gameObject);
                OnPropertyClicked(index);
            }
            else if (obj.name.StartsWith("DockPropertyItem"))
            {
                var index = sceneComponent.DockGroup.GetItemIndex(obj.transform.parent.gameObject);
                OnDockItemClicked(index);
            }
            else if (obj.name == "btnSubmit")
            {
                //PillController.instance.AddPill(new PillModelData(CurrentSelectShapeVO, new List<MedicinalProperties>(currentSelectProperties)));
                PillController.instance.CreatePillEntity(new PillModelData(CurrentSelectShapeVO, CurrentSelectSizeVO, new List<MedicinalProperties>(currentSelectProperties)));
                OnShow();
            }
        }

        private void RefreshShapes()
        {
            var configs = unlockedShapeConfigs;
            sceneComponent.shapeGroup.SetSize(configs.Count);
            for (int i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                var item = sceneComponent.shapeGroup.GetItem<ShapeItem>(i);
                item.SetData(config);
                item.SetSelect(currentSelectShapeIndex == i);
            }
            ResetAllClicks();
        }

        private void RefreshSize()
        {
            var configs = unlockedSizeConfigs;
            sceneComponent.SizeGroup.SetSize(configs.Count);
            for (int i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                var item = sceneComponent.SizeGroup.GetItem<SizeItem>(i);
                item.SetData(config);
                item.SetSelect(currentSelectSizeIndex == i);
            }
            ResetAllClicks();
        }

        private void RefreshProperties()
        {
            var configs = unlockedPropertyConfigs;
            sceneComponent.PropertyGroup.SetSize(configs.Count);
            for (int i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                var item = sceneComponent.PropertyGroup.GetItem<PropertyItem>(i);
                item.SetData(config);
            }
            ResetAllClicks();
        }

        private void RefreshDocks()
        {
            var sizeVO = CurrentSelectSizeVO;
            if (sizeVO == null)
            {
                sceneComponent.DockGroup.SetSize(0);
                return;
            }
            else
            {
                sceneComponent.DockGroup.SetSize(sizeVO.cellSize);
                for (int i = 0; i < sizeVO.cellSize; i++)
                {
                    var item = sceneComponent.DockGroup.GetItem<DockItem>(i);
                    if (i < currentSelectProperties.Count)
                    {
                        item.SetData(currentSelectProperties[i]);
                    }
                    else
                    {
                        item.SetData(null);
                    }
                }
            }
            ResetAllClicks();
        }

        private void OnSizeItemClicked(int index)
        {
            if (CurrentSelectShapeVO == null)
            {
                TipsController.instance.ShowTips($"请先选择一个形状");
                return;
            }
            var newSizeVO = unlockedSizeConfigs[index];
            if (currentSelectProperties.Count > newSizeVO.cellSize)
            {
                TipsController.instance.ShowMsgBox(new MsgBoxUIData()
                {
                    content = "您当前选择的形状尺寸小于已经放置的药物成分，多余的成分会被清除掉\n是否继续？",
                    isShowCancelBtn = true,
                    callbackOK = () =>
                    {
                        DoChangeSizeIndex(index);
                    },
                });
                return;
            }
            else
            {
                DoChangeSizeIndex(index);
            }
        }

        private void OnShapeItemClicked(int index)
        {
            DoChangeShapeIndex(index);
        }

        private void DoChangeShapeIndex(int newShapeIndex)
        {
            sceneComponent.PlayMakePillSound();
            currentSelectShapeIndex = newShapeIndex;
            RefreshShapes();
            RefreshButton();
        }

        private void DoChangeSizeIndex(int newSizeIndex)
        {
            sceneComponent.PlayMakePillSound();
            currentSelectSizeIndex = newSizeIndex;
            if (currentSelectProperties.Count > CurrentSelectSizeVO.cellSize)
            {
                currentSelectProperties.RemoveRange(CurrentSelectSizeVO.cellSize, currentSelectProperties.Count - CurrentSelectSizeVO.cellSize);
            }
            RefreshSize();
            RefreshDocks();
            RefreshButton();
        }

        private void OnPropertyClicked(int index)
        {
            if (CurrentSelectSizeVO == null)
            {
                TipsController.instance.ShowTips("请先选择一个大小吧");
                return;
            }
            if (currentSelectProperties.Count >= CurrentSelectSizeVO.cellSize)
            {
                TipsController.instance.ShowTips("已经放不下了");
                return;
            }

            AddPropertyToSelectList(unlockedPropertyConfigs[index]);
            RefreshDocks();
            RefreshButton();

            sceneComponent.PlayMakePillSound();
        }

        private void AddPropertyToSelectList(MedicinalProperties selectProperty)
        {
            for (int i = 0; i < currentSelectProperties.Count; i++)
            {
                var property = currentSelectProperties[i];
                if (property == selectProperty)
                {
                    currentSelectProperties.Insert(i, selectProperty);
                    return;
                }
            }
            currentSelectProperties.Add(selectProperty);
        }

        private bool IsSubmitBtnValid()
        {
            if (currentSelectShapeIndex < 0)
            {
                return false;
            }

            if (currentSelectSizeIndex < 0)
            {
                return false;
            }

            if (currentSelectProperties.Count == 0)
            {
                return false;
            }

            // if (!isDrawDown)
            // {
            //     return false;
            // }
            return true;
        }

        private void RefreshButton()
        {
            sceneComponent.btnSubmit.interactable = IsSubmitBtnValid();
        }

        private void OnDockItemClicked(int index)
        {
            currentSelectProperties.RemoveAt(index);
            RefreshDocks();
            RefreshButton();
            sceneComponent.PlayMakePillSound();
        }

        private void OnLevelUpgrade(int newLevel)
        {
            RefreshUnlockItems(false);
            RefreshShapes();
            RefreshSize();
            RefreshProperties();
        }

        //private bool isDrawDown = false;
        private void OnDrawDown()
        {
            //isDrawDown = true;
            //RefreshButton();
            PillController.instance.CreatePillEntity(new PillModelData(CurrentSelectShapeVO, CurrentSelectSizeVO, new List<MedicinalProperties>(currentSelectProperties)));
            ResetSignArea();
            RefreshUnlockItems(false);
            ResetViewData();
            RefreshShapes();
            RefreshSize();
            RefreshProperties();
            RefreshDocks();
            RefreshButton();
        }

        private bool CanDraw()
        {
            if (fadeTween != null && !fadeTween.IsComplete()) return false;
            return IsSubmitBtnValid();
        }

        private Sequence fadeTween;
        private void ResetSignArea()
        {
            if (fadeTween == null)
            {
                fadeTween = DOTween.Sequence()
                .Append(sceneComponent.drawLineBehavior.targetImage.DOFade(0, 2.0f))
                .AppendCallback(() =>
                {
                    sceneComponent.drawLineBehavior.targetImage.color = Color.white;
                    sceneComponent.drawLineBehavior.Reset();
                })
                .SetAutoKill(false);
            }
            else
            {
                fadeTween.Complete();
                fadeTween.Restart();
            }

            //isDrawDown = false;
        }
    }
}