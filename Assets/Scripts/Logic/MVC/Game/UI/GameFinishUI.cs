using UnityEngine;
using DG.Tweening;

namespace Logic
{
    public class GameFinishUI : BaseUI
    {
        protected override UISort sort => UISort.GameFinishUI;

        protected override string prefabName => "Game/GameFinishUI";
        private Sequence showSequence;

        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "btnRestart")
            {
                GameController.instance.ReStartCurrentLevel();
            }
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {
            if (showSequence != null)
            {
                showSequence.Kill();
                showSequence = null;
            }
        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            bool success = (bool)data;
            sceneObject.transform.Find("success").gameObject.SetActiveOptimize(success);
            sceneObject.transform.Find("fail").gameObject.SetActiveOptimize(!success);
            showSequence = DOTween.Sequence()
                .Append(sceneObject.transform.DOScale(1, 1.0f).From(0.5f).SetEase(Ease.OutBack))
                .Join(sceneObject.transform.GetComponent<CanvasGroup>().DOFade(1, 1.0f).From(0));
            AudioManager.instance.PlayEffect(success ? "游戏胜利2" : "游戏失败");
        }
    }
}