using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class PropertyItem : MonoBehaviour
    {
        public Image bg;
        public Image icon;
        public Button btn;
        public Text txtName;
        private MedicinalProperties property;
        void Start()
        {
            EventTriggerListener.Get(btn.gameObject).onEnter = OnEnter;
            EventTriggerListener.Get(btn.gameObject).onExit = OnExit;
        }

        public void SetData(MedicinalProperties property)
        {
            this.property = property;
            txtName.text = property.name;
            bg.sprite = Resources.Load<Sprite>($"Sprites/UI/便签0{Random.Range(3, 6)}");
            icon.sprite = Resources.Load<Sprite>($"Sprites/Property/{property.assetName}");
            icon.SetNativeSize();
            txtName.text = property.name;
        }

        void OnEnter(GameObject obj)
        {
            if (property != null && property.desc.IsFilled())
            {
                TipsController.instance.ShowUI(UISort.ItemDescTipsUI, property.desc);
            }
        }

        void OnExit(GameObject obj)
        {
            TipsController.instance.HideUI(UISort.ItemDescTipsUI);
        }
    }
}