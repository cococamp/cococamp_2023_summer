using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class PrescriptionUIComp : MonoBehaviour
    {
        public MultiItemManager shapeGroup;
        public MultiItemManager SizeGroup;
        public MultiItemManager PropertyGroup;
        public MultiItemManager DockGroup;
        public Button btnSubmit;
        public DrawLineBehaviour drawLineBehavior;
        public AudioSource signSound;
        public AudioSource makePillSound;

        private float countDown = 0;
        public void PlayMakePillSound()
        {
            countDown = 2.0f;
            makePillSound.volume = 1.0f;
        }
        public void Update()
        {
            if (countDown > 0)
            {
                countDown -= Time.deltaTime;
                makePillSound.volume = countDown / 2.0f;
            }
        }
    }
}