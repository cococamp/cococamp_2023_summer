using UnityEngine;
using UnityEngine.UI;
using Config;
using System;
using UnityEngine.Events;

namespace Logic
{
    public class SizeItem : MonoBehaviour
    {
        public Image icon;
        public Text txtName;
        public Image select;
        private SizeVO sizeVO;

        void Start()
        {
            EventTriggerListener.Get(gameObject).onEnter = OnEnter;
            EventTriggerListener.Get(gameObject).onExit = OnExit;
        }

        public void SetData(SizeVO sizeVO)
        {
            icon.sprite = Resources.Load<Sprite>($"Sprites/Size/{sizeVO.assetName}");
            icon.SetNativeSize();
            txtName.text = sizeVO.name;
            this.sizeVO = sizeVO;
        }

        public void SetSelect(bool value)
        {
            select.gameObject.SetActiveOptimize(value);
        }

        void OnEnter(GameObject obj)
        {
            if (sizeVO != null && sizeVO.desc.IsFilled())
            {
                TipsController.instance.ShowUI(UISort.ItemDescTipsUI, sizeVO.desc);
            }
        }

        void OnExit(GameObject obj)
        {
            TipsController.instance.HideUI(UISort.ItemDescTipsUI);
        }
    }
}