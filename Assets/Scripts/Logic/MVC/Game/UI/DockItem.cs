using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class DockItem : MonoBehaviour
    {
        public Image icon;
        public Text txtName;
        public GameObject propertyRoot;
        private MedicinalProperties property;
        void Start()
        {
            EventTriggerListener.Get(gameObject).onEnter = OnEnter;
            EventTriggerListener.Get(gameObject).onExit = OnExit;
        }

        public void SetData(MedicinalProperties property)
        {
            if (property != null)
            {
                propertyRoot.SetActiveOptimize(true);
                txtName.text = property.name;
                icon.sprite = Resources.Load<Sprite>($"Sprites/Property/{property.assetName}");
            }
            else
            {
                propertyRoot.SetActiveOptimize(false);
            }
            this.property = property;
        }

        void OnEnter(GameObject obj)
        {
            if (property != null && property.desc.IsFilled())
            {
                TipsController.instance.ShowUI(UISort.ItemDescTipsUI, property.desc);
            }
        }

        void OnExit(GameObject obj)
        {
            TipsController.instance.HideUI(UISort.ItemDescTipsUI);
        }
    }
}