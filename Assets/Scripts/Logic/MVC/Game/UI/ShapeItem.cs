using UnityEngine;
using UnityEngine.UI;
using Config;
using System;
using UnityEngine.Events;

namespace Logic
{
    public class ShapeItem : MonoBehaviour
    {
        public Image icon;
        public Text txtName;
        public Image select;
        private ShapeVO shapeVO;

        void Start()
        {
            EventTriggerListener.Get(gameObject).onEnter = OnEnter;
            EventTriggerListener.Get(gameObject).onExit = OnExit;
        }

        public void SetData(ShapeVO shapeVO)
        {
            icon.sprite = Resources.Load<Sprite>($"Sprites/Shape/{shapeVO.assetName}");
            icon.SetNativeSize();
            txtName.text = shapeVO.name;
            this.shapeVO = shapeVO;
        }

        public void SetSelect(bool value)
        {
            select.gameObject.SetActiveOptimize(value);
        }

        void OnEnter(GameObject obj)
        {
            if (shapeVO != null && shapeVO.desc.IsFilled())
            {
                TipsController.instance.ShowUI(UISort.ItemDescTipsUI, shapeVO.desc);
            }
        }

        void OnExit(GameObject obj)
        {
            TipsController.instance.HideUI(UISort.ItemDescTipsUI);
        }
    }
}