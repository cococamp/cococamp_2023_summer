using UnityEngine;
using UnityEngine.InputSystem;

namespace Logic
{
    public class PillObjectEntityComp : MonoBehaviour
    {
        public Animator animator;
        public GameObject bubbleEffect;
        public PillObjectEntity entity;
        public GameObject normalObj;
        public GameObject brokenObj;
        public AudioSource audioSource;
        private bool isDragging = false;
        private float z = 3.66f;
        private float _alpha = 1.0f;
        public float Alpha
        {
            get => _alpha;
            set
            {
                _alpha = value;
                SetAlpha(normalObj, value);
                SetAlpha(brokenObj, 1 - value);
            }
        }
        void Start()
        {
            InstantiateAllMaterial();
        }

        void OnMouseDown()
        {
            isDragging = true;
            transform.GetComponent<Rigidbody>().isKinematic = true;
            GameObject.FindObjectOfType<BodyComp>().animator.Play("MouthOpen");
        }

        void OnMouseUp()
        {
            isDragging = false;
            transform.GetComponent<Rigidbody>().isKinematic = false;
        }

        void Update()
        {
            if (isDragging)
            {
                var screenPos = Cameras.CameraManager.instance.sceneCamera.cameraComponent.WorldToScreenPoint(transform.position);
                var mousePosition = Mouse.current.position.ReadValue();
                Vector3 mouseWorldPosition = new Vector3(mousePosition.x, mousePosition.y, screenPos.z);
                var worldPosition = Cameras.CameraManager.instance.sceneCamera.cameraComponent.ScreenToWorldPoint(mouseWorldPosition);
                //Debug.LogError($"screenPos {screenPos} mousePosition {mousePosition} mouseWorldPosition {mouseWorldPosition} worldPosition {worldPosition}");
                transform.position = new Vector3(worldPosition.x, worldPosition.y, z);
            }
            else if (transform.position.y < -100)
            {
                entity.Destroy();
            }
        }

        private void InstantiateAllMaterial()
        {
            var renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
            foreach (var render in renderers)
            {
                Material[] newMats = new Material[render.materials.Length];
                for (int i = 0; i < render.materials.Length; i++)
                {
                    var mat = render.materials[i];
                    newMats[i] = GameObject.Instantiate<Material>(mat);
                }
                render.materials = newMats;
            }

        }

        private void SetAlpha(GameObject gameObject, float alpha)
        {
            var renderers = gameObject.GetComponentsInChildren<MeshRenderer>();
            foreach (var render in renderers)
            {
                foreach (var mat in render.materials)
                {
                    var color = mat.color;
                    color.a = alpha;
                    mat.color = color;
                }
            }
        }

        void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.tag == "Panzi")
            {
                AudioManager.instance.PlayEffect("药丸掉落");
            }
        }
    }
}