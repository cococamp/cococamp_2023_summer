using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using DG.Tweening;

namespace Logic
{
    public class PillObjectEntity : BaseEntity<PillObjectEntityComp, PillModelData>
    {
        public PillModelData Data => data;
        private Sequence animSequence;
        public bool IsAnim { get; private set; }
        private float Scale => data.sizeVo.pillObjectScale;
        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            sceneComponent.GetComponent<Rigidbody>().velocity = Vector3.zero;
            sceneObject.transform.position = new Vector3(-25.13f, 28.39f, 3.66f);
            sceneComponent.entity = this;
            sceneComponent.bubbleEffect.SetActiveOptimize(false);
            sceneComponent.animator.enabled = false;
            sceneComponent.transform.localScale = new Vector3(Scale, Scale, Scale);
            sceneComponent.GetComponent<Collider>().isTrigger = false;
            sceneComponent.Alpha = 1;
            sceneComponent.audioSource.volume = 0f;
            sceneComponent.audioSource.Stop();
            IsAnim = false;
        }
        protected override void OnHide()
        {

        }
        protected override void OnDestroy()
        {
            if (animSequence != null)
            {
                animSequence.Kill();
                animSequence = null;
            }
            sceneComponent.entity = null;
            sceneComponent.GetComponent<Rigidbody>().isKinematic = false;
            sceneComponent.animator.Play("Idle");
            sceneComponent.bubbleEffect.SetActiveOptimize(false);
            sceneComponent.animator.enabled = false;
            sceneComponent.transform.localScale = new Vector3(Scale, Scale, Scale);
            sceneComponent.GetComponent<Collider>().isTrigger = false;
            sceneComponent.Alpha = 1;
            sceneComponent.audioSource.volume = 0f;
            sceneComponent.audioSource.Stop();
            IsAnim = false;
        }
        protected override void OnClick(GameObject obj)
        {

        }

        private Vector3 animStartPos = new Vector3(7.4f, 47.61f, 1.36f);
        public void DoAnim()
        {
            IsAnim = true;
            sceneComponent.GetComponent<Rigidbody>().isKinematic = true;
            sceneComponent.animator.enabled = false;
            sceneComponent.GetComponent<Collider>().isTrigger = true;
            if (animSequence != null)
            {
                animSequence.Kill();
                animSequence = null;
            }
            animSequence = DOTween.Sequence()
                                    .Append(sceneObject.transform.DOMove(animStartPos, 1.0f))
                                    .AppendCallback(() =>
                                    {
                                        sceneComponent.animator.enabled = true;
                                        sceneComponent.animator.Play("Move");
                                        GameObject.FindObjectOfType<BodyComp>().animator.Play("CloseMouth");
                                    })
                                    .AppendInterval(3.1f)
                                    .AppendCallback(() =>
                                    {
                                        PillController.instance.AddPill(data);
                                        sceneComponent.bubbleEffect.SetActiveOptimize(true);
                                        sceneComponent.audioSource.volume = 0.5f;
                                        sceneComponent.audioSource.Play();
                                    })

                                    .Append(DOTween.To(() => sceneComponent.Alpha, (value) => sceneComponent.Alpha = value, 0, data.sizeVo.lifeTime * 0.3f))
                                    .Append(sceneComponent.transform.DOScale(0, data.sizeVo.lifeTime * 0.7f))
                                    .Join(sceneComponent.audioSource.DOFade(0, data.sizeVo.lifeTime * 0.7f))
                                    //.AppendInterval(data.shapeVo.LifeTime)
                                    .AppendCallback(() =>
                                    {
                                        PillController.instance.DestroyEntity(this);
                                    });
        }
    }
}