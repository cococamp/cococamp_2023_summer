using UnityEngine;

namespace Logic
{
    public class AddPillTriggerAreaComp : MonoBehaviour
    {
        void OnTriggerEnter(Collider other)
        {
            var pillComp = other.gameObject.GetComponent<PillObjectEntityComp>();
            if (pillComp != null && !pillComp.entity.IsAnim)
            {
                pillComp.entity.DoAnim();
            }
        }
    }
}