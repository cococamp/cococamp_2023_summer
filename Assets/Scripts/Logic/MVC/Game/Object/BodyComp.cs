using UnityEngine;
using System.Collections.Generic;
using Config;
using System;
using System.Linq;

namespace Logic
{
    [Serializable]
    public class OrganObjectData
    {
        public eOrganType type;
        public OrganStateObjects[] stateObjects;
    }

    [Serializable]
    public class OrganHurtEffectData
    {
        public eOrganType type;
        public Renderer[] renderers;
        public Color recoverColor;
        public Color hurtColor;
    }
    
    [Serializable]
    public class OrganStateObjects
    {
        public eOrganState state;
        public GameObject[] objects;
    }
    public class BodyComp : MonoBehaviour
    {
        public Animator animator;
        public OrganObjectData[] organs;
        public OrganHurtEffectData[] organEffectDatas;
        
        void Start()
        {
            foreach (var organ in organs)
            {
                if (organ != null)
                {
                    foreach (var state in organ.stateObjects)
                    {
                        if (state != null)
                        {
                            foreach (var obj in state.objects)
                            {
                                if (obj != null)
                                {
                                    obj.SetActiveOptimize(false);
                                }
                            }
                        }
                    }
                }
            }
            animator.Play("Stable");
        }

        void Update()
        {
            if (GameController.instance.operatorManager.currentState == GameOperatorState.Level)
            {
                RefreshOrganState();
            }
        }

        private void RefreshOrganState()
        {
            foreach (var organData in OrganController.instance.Model.OrganDict)
            {
                foreach (var organ in organs)
                {
                    if (organ.type == organData.Key)
                    {
                        var currentState = organData.Value.State;
                        foreach (var state in organ.stateObjects)
                        {
                            foreach (var obj in state.objects)
                            {
                                obj.SetActiveOptimize(state.state == currentState);
                            }
                        }
                    }

                    if (organ.type == organData.Key && organ.type != eOrganType.Stomach)
                    {
                        RefreshOrganEffect(organ.type, organData.Value);
                    }
                }
            }
        }

        
        private void RefreshOrganEffect(eOrganType organType,OrganModelData organModelData)
        {
            var effectValue = organModelData.effectValue;
            var organEffectData = organEffectDatas.First(o => o.type == organType);
            bool isRecover =  effectValue > 0;
            int enableValue = Mathf.Abs(effectValue) > 0.0000001f ? 1 : 0;
            float flashSpeed = 3.3f;
            foreach (var render in organEffectData.renderers)
            {
                Material material = render.material;
                material.SetColor("_OutlineColor", isRecover ? organEffectData.recoverColor : organEffectData.hurtColor);
                material.SetInt("_EnableFlashing",enableValue);
                material.SetFloat("_FlashSpeed",flashSpeed);
            }
            
        }
    }
}