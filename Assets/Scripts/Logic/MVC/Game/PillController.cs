using System.Collections;
using System.Collections.Generic;
using Config;
using UnityEngine;

namespace Logic
{
    public class PillController : BaseController<PillController, PillModel>
    {
        protected override void OnUpdate()
        {
            base.OnUpdate();

            Model.Update(Time.deltaTime);
        }

        public void AddPill(PillModelData pillData)
        {
            Model.AddPill(pillData);
        }

        public float GetPillEffectValue(eOrganType organType)
        {
            return Model.GetPillEffectValue(organType);
        }

        private List<BaseEntity> entityList = new List<BaseEntity>();
        public void CreatePillEntity(PillModelData data)
        {
            PillObjectEntity newPillObjectEntity = new PillObjectEntity();
            newPillObjectEntity.Init(data.shapeVo.prefabName);
            newPillObjectEntity.Show(data);
            entityList.Add(newPillObjectEntity);
        }

        public void DestroyEntity(BaseEntity entity)
        {
            entity.Destroy();
            entityList.Remove(entity);
        }

        public void DestroyAllEntities()
        {
            foreach (var item in entityList)
            {
                item.Destroy();
            }
            entityList.Clear();
        }
    }
}


