using System.Collections;
using System.Collections.Generic;
using Config;
using Logic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Logic
{
    public class OrganModel : BaseModel
    {
        private Dictionary<eOrganType, OrganModelData> m_organDict;
        public IReadOnlyDictionary<eOrganType, OrganModelData> OrganDict => m_organDict;
        public bool IsFinished { get; private set; }

        public override void Init()
        {


        }

        public void SetFinished()
        {
            IsFinished = true;
        }

        public void InitData()
        {
            // 初始化数据
            m_organDict = new Dictionary<eOrganType, OrganModelData>();
            var organConfigList = ConfigManager.instance.Get<OrganConfig>().organConfigList;
            foreach (var organVo in organConfigList)
            {
                m_organDict[organVo.organType] = new OrganModelData()
                {
                    MaxHP = organVo.HP,
                    HP = organVo.initHP,
                    config = organVo,
                };
            }
            IsFinished = false;
        }

        public void Update(eOrganType organType, float effectValue)
        {
            if (!m_organDict.ContainsKey(organType))
            {
                Debug.LogError($"没有这个器官{organType}");
                return;
            }

            var organData = m_organDict[organType];
            organData.effectValue = effectValue;
            var changeValue = effectValue * Time.deltaTime;
            if (changeValue > 0)
            {
                organData.HP = Mathf.Min(organData.HP + changeValue, organData.MaxHP);
            }
            else
            {
                organData.HP = Mathf.Max(organData.HP + changeValue, 0);
            }
            ExpController.instance.AddExp(changeValue);
        }

        public void CheckOrganHealthEnd()
        {

        }

        public override void Destroy()
        {
            m_organDict.Clear();
        }
    }

    public class OrganModelData
    {
        public float HP;
        public float MaxHP;
        public float effectValue;
        public OrganVO config;

        private eOrganState _state = eOrganState.NotValid;
        public eOrganState State
        {
            get
            {
                if (_state == eOrganState.NotValid)
                {
                    if (HP <= MaxHP * 0.2f)
                        _state = eOrganState.Dying;
                    else if (HP < MaxHP * 0.7f)
                        _state = eOrganState.Blooding;
                    else
                        _state = eOrganState.Health;
                }
                else if (_state == eOrganState.Health)
                {
                    if (HP < (MaxHP * 0.7f - 1))
                    {
                        _state = eOrganState.Blooding;
                    }
                }
                else if (_state == eOrganState.Blooding)
                {
                    if (HP < (MaxHP * 0.2f - 1))
                        _state = eOrganState.Dying;
                    else if (HP > (MaxHP * 0.7f + 1))
                        _state = eOrganState.Health;
                }
                else if (_state == eOrganState.Dying)
                {
                    if (HP > (MaxHP * 0.2f + 1))
                    {
                        _state = eOrganState.Blooding;
                    }
                }
                return _state;
            }
        }

        public float GetCurrentSelfHPChangeSpeed()
        {
            var state = State;
            foreach (var item in config.organStateList)
            {
                if (item.state == state)
                {
                    return item.changeValue;
                }
            }
            return 0;
        }
    }

    public enum eOrganState
    {
        [LabelText("非法状态")]
        NotValid,   // 非法状态
        [LabelText("健康状态")]
        Health,    //健康状态 不掉血 且血量高于70%
        [LabelText("掉血状态")]
        Blooding,       // 掉血状态
        [LabelText("濒死状态")]
        Dying,       // 濒死状态
    }
}

