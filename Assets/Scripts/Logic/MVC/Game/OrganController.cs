using System;
using System.Collections;
using System.Collections.Generic;
using Config;
using UnityEngine;

namespace Logic
{
    public class OrganController : BaseController<OrganController, OrganModel>
    {
        protected override void OnUpdate()
        {
            if (GameController.instance.operatorManager.currentState != GameOperatorState.Level) return;
            if (Model.IsFinished) return;

            // 器官数据
            foreach (var config in ConfigManager.instance.Get<OrganConfig>().organConfigList)
            {
                var organType = config.organType;
                var effectValue = PillController.instance.GetPillEffectValue(organType);
                var selfChangeValue = Model.OrganDict[organType].GetCurrentSelfHPChangeSpeed();
                Model.Update(organType, effectValue + selfChangeValue);
            }

            CheckGameFinish();
        }

        private void CheckGameFinish()
        {
            bool isAllHealth = true;
            foreach (var item in Model.OrganDict)
            {
                var organData = item.Value;
                if (organData.HP <= 0)
                {
                    Model.SetFinished();
                    // TipsController.instance.ShowMsgBox(new MsgBoxUIData()
                    // {
                    //     content = $"{organData.config.name}坏死 游戏结束",
                    //     isShowCancelBtn = false,
                    //     callbackOK = () =>
                    //     {
                    //         //GameController.instance.ChangeToIntro();
                    //     }
                    // });
                    GameController.instance.ShowUI(UISort.GameFinishUI, false);
                    return;
                }

                if (organData.State != eOrganState.Health)
                {
                    isAllHealth = false;
                }
            }

            if (isAllHealth)
            {
                Model.SetFinished();
                // TipsController.instance.ShowMsgBox(new MsgBoxUIData()
                // {
                //     content = $"全部器官都健康 游戏胜利",
                //     isShowCancelBtn = false,
                //     callbackOK = () =>
                //     {
                //         //GameController.instance.ChangeToIntro();
                //     }
                // });
                GameController.instance.ShowUI(UISort.GameFinishUI, true);
                return;
            }
        }
    }
}

