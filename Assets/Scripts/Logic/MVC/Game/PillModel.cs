using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Config;
using UnityEngine;

namespace Logic
{
    public class PillModel : BaseModel
    {
        private List<PillModelData> m_pillList;
        public IList<PillModelData> PillList => m_pillList;

        public override void Init()
        {
            m_pillList = new List<PillModelData>();
        }

        public void InitData()
        {
            m_pillList = new List<PillModelData>();
        }

        public void Update(float deltaTime)
        {
            if (m_pillList is null)
                return;

            var needRemovePillList = new List<PillModelData>();
            foreach (var pillData in m_pillList)
            {
                pillData.currentLifeTime += deltaTime;
                if (pillData.currentLifeTime >= pillData.sizeVo.lifeTime)
                {
                    needRemovePillList.Add(pillData);
                }
            }

            // 删除过期药物
            foreach (var pillData in needRemovePillList)
            {
                m_pillList.Remove(pillData);
            }
        }

        public override void Destroy()
        {
            m_pillList.Clear();
        }

        // 药物添加
        public void AddPill(PillModelData pillData)
        {
            if (pillData is null)
                return;
            m_pillList.Add(pillData);
        }

        // 获取药物最终数据值
        public float GetPillEffectValue(eOrganType organType)
        {
            if (m_pillList is null || m_pillList.Count == 0)
                return 0;

            float totalEffect = 0;
            foreach (var pilldata in m_pillList)
            {
                totalEffect += pilldata.GetEffectValue(organType);
            }

            return totalEffect;
        }
    }

    public class PillModelData
    {
        public float currentLifeTime;   // 现在存活时间
        public ShapeVO shapeVo;     // 形状数据
        public SizeVO sizeVo;     // 形状数据
        public List<MedicinalProperties> medicinalPropertiesList;   // 药性数据
        public FormulaConfigVO formulaConfigVO;         // 配方数据

        public float GetEffectValue(eOrganType organType)
        {
            // 获取影响数值
            var baseSpeed = shapeVo.baseSpeed;
            var addSpeed = shapeVo.curve.Evaluate(currentLifeTime / sizeVo.lifeTime * shapeVo.CurveLifeTime);

            float propertyValue = 0;
            foreach (var property in medicinalPropertiesList)
            {
                foreach (var effect in property.effectList)
                {
                    if (effect.organType == organType)
                    {
                        propertyValue += effect.effectValue;
                    }
                }
            }
            var formulaSpeed = propertyValue > 0 ? (formulaConfigVO?.formulaValue ?? 1.0f) : 1.0f;
            return propertyValue * baseSpeed * addSpeed * formulaSpeed;
        }

        public PillModelData(ShapeVO _shapeVo, SizeVO sizeVO, List<MedicinalProperties> _medicinalPropertiesList)
        {
            currentLifeTime = 0;
            shapeVo = _shapeVo;
            this.sizeVo = sizeVO;
            medicinalPropertiesList = _medicinalPropertiesList;
            formulaConfigVO = GetMatchFormula(_medicinalPropertiesList);
        }

        //配方记录重开暂时不用重置
        private static List<FormulaConfigVO> gatherFormulas = new List<FormulaConfigVO>();

        private FormulaConfigVO GetMatchFormula(List<MedicinalProperties> _medicinalPropertiesList)
        {
            var formulaConfigs = ConfigManager.instance.Get<FormulaConfig>().formulaConfigList;
            foreach (var formula in formulaConfigs)
            {
                var pillProperties = medicinalPropertiesList.Select(config => config.name).Distinct();
                bool isMatch = true;
                foreach (var formulaProperty in formula.propertiesList)
                {
                    if (!pillProperties.Contains(formulaProperty))
                    {
                        isMatch = false;
                        break;
                    }
                }

                if (isMatch)
                {
                    //必须要完全匹配才可以  不能多不能少
                    foreach (var pillProperty in pillProperties)
                    {
                        if (!formula.propertiesList.Contains(pillProperty))
                        {
                            isMatch = false;
                            break;
                        }
                    }
                }

                if (isMatch)
                {
                    if (!gatherFormulas.Contains(formula))
                    {
                        gatherFormulas.Add(formula);
                        TipsController.instance.ShowMsgBox(new MsgBoxUIData()
                        {
                            content = $"达成配方:{formula.name}\n{formula.desc}",
                            isShowCancelBtn = false,
                            callbackOK = null,
                        });
                    }
                    return formula;
                }
            }
            return null;
        }

        private static Dictionary<MedicinalProperties, int> propertyLogDict = new Dictionary<MedicinalProperties, int>();
        public string GetPropertyLog()
        {
            propertyLogDict.Clear();
            foreach (var item in medicinalPropertiesList)
            {
                if (propertyLogDict.ContainsKey(item))
                {
                    propertyLogDict[item]++;
                }
                else
                {
                    propertyLogDict.Add(item, 1);
                }
            }
            string log = "";
            foreach (var pair in propertyLogDict)
            {
                log += $"{pair.Key.name}*{pair.Value}、";
            }
            return log;
        }

        private static Dictionary<OrganVO, float> effectLogDict = new Dictionary<OrganVO, float>();
        public string GetEffectLog()
        {
            effectLogDict.Clear();
            foreach (var organConfig in ConfigManager.instance.OrganList)
            {
                effectLogDict.Add(organConfig, GetEffectValue(organConfig.organType));
            }
            string log = "";
            foreach (var pair in effectLogDict)
            {
                log += $"{pair.Key.name}*{pair.Value.ToString("F2")}、";
            }
            return log;
        }
    }
}

