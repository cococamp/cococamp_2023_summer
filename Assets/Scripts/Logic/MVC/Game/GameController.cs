using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Config;

namespace Logic
{
    public class GameController : BaseController<GameController, GameModel>
    {
        private bool _pauseGame = false;
        public bool PauseGame
        {
            get => _pauseGame;
            set
            {
                if (_pauseGame != value)
                {
                    _pauseGame = value;
                }
            }
        }
        public GameOperatorManager operatorManager;
        protected override void OnInit()
        {
            operatorManager = new GameOperatorManager();
            InitDebugUI();
        }

        protected override void OnDestroy()
        {
            DestroyDebugUI();
        }
        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.IntroUI, new IntroUI());
            RegisterUI(UISort.GameUI, new GameUI());
            RegisterUI(UISort.PrescriptionUI, new PrescriptionUI());
            RegisterUI(UISort.GameFinishUI, new GameFinishUI());
        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().AddListener(OnModulesInitFinished);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().RemoveListener(OnModulesInitFinished);
        }

        protected override void OnFixedUpdate()
        {
            Model.FixedUpdate();
        }

        private void OnButtonClick(InputAction.CallbackContext context)
        {
            if (context.action.name == "Escape")
            {
                OpenGameUI();
            }
        }

        private void OpenGameUI()
        {
            //提交分数的时候  不能打开这个界面
            if (LeaderBoardController.instance.GetUI(UISort.InputNameUI)?.isShow == false)
            {
                if (GetUI<GameUI>(UISort.GameUI).isShow)
                {
                    HideUI(UISort.GameUI);
                }
                else
                {
                    ShowUI(UISort.GameUI);
                }
            }
        }

        private void OnModulesInitFinished()
        {
            operatorManager.Init();
        }

        public void ReStartCurrentLevel()
        {
            LoadLevel(SceneController.instance.currentSceneID);
        }

        public void LoadLevel(string levelID)
        {
            operatorManager.ChangeState(GameOperatorState.Level, new GameOperatorParam_Level() { levelID = levelID });
        }

        public void ChangeToIntro()
        {
            operatorManager.ChangeState(GameOperatorState.Intro);
        }

        protected override void OnUpdate()
        {
            if (Keyboard.current.escapeKey.wasPressedThisFrame)
            {
                OpenGameUI();
            }
        }

        #region Debug
        private DebugUI debugUI;
        private void InitDebugUI()
        {
            debugUI = DebugController.instance.CreateDebugUI(() =>
            {
                List<string> logs = new List<string>()
                {
                    $"当前等级:{ExpController.instance.Model.PlayerLevel} 经验值:{ExpController.instance.Model.CurrentExp.ToString("F2")}/{ExpController.instance.Model.NextLevelExp.ToString("F2")}",
                };
                if (operatorManager != null && operatorManager.currentState == GameOperatorState.Level)
                {
                    //器官数据
                    foreach (var organ in ConfigManager.instance.OrganList)
                    {
                        var data = OrganController.instance.Model.OrganDict[organ.organType];
                        var hp = string.Format("{0:0}", data.HP);
                        logs.Add($"{organ.name} State:{data.State} HP:{data.HP.ToString("F2")}/{data.MaxHP}  ChangSpeed:{data.effectValue.ToString("F2")} ");
                    }

                    //药物数据
                    for (int i = 0; i < PillController.instance.Model.PillList.Count; i++)
                    {
                        var pillData = PillController.instance.Model.PillList[i];
                        logs.Add($"{pillData.GetPropertyLog()}  Lift:{pillData.currentLifeTime.ToString("F2")}/{pillData.sizeVo.lifeTime.ToString("F2")} Formula:{pillData.formulaConfigVO?.name ?? "无"} \n药效:{pillData.GetEffectLog()}");
                    }
                }

                return string.Join("\n", logs);
            }, new Vector2(0, 0));
        }

        private void DestroyDebugUI()
        {
            DebugController.instance.DestroyDebugUI(debugUI);
            debugUI = null;
        }

        #endregion
    }
}