using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public static class Vector2IntExtension
    {
        public static int ManhattanDistance(this Vector2Int from, Vector2Int to)
        {
            return Mathf.Abs(from.x - to.x) + Mathf.Abs(from.y - to.y);
        }

        /// <summary>
        /// 将向量沿顺时针旋转90度count次
        /// </summary>
        public static Vector2Int Rotate(this Vector2Int instance, int count)
        {
            while (count < 0)
            {
                count += 4;
            }
            Vector2Int dir = new Vector2Int((count % 4 == 2 ? -1 : 1) * (count % 2 == 0 ? 1 : 0), (count % 4 == 1 ? -1 : 1) * (count % 2));
            int x = instance.x * dir.x - instance.y * dir.y;
            int y = instance.x * dir.y + instance.y * dir.x;
            return new Vector2Int(x, y);
        }

        /// <summary>
        /// 将向量旋转,旋转度数为从方向(0,1)到dir经过的角度
        /// </summary>
        public static Vector2Int Rotate(this Vector2Int instance, Vector2Int dir)
        {
            int x = instance.x * dir.y + instance.y * dir.x;
            int y = instance.y * dir.y - instance.x * dir.x;
            return new Vector2Int(x, y);
        }

        public enum eDirection
        {
            None,
            Left,
            Right,
            Up,
            Down,
        }

        /// <summary>
        /// 将向量旋转,旋转度数为从上方向到dir方向经过的角度
        /// </summary>
        public static Vector2Int Rotate(this Vector2Int instance, eDirection dir)
        {
            return instance.Rotate(Vector2IntExtension.ParseDirection(dir));
        }

        public static Vector2Int FlipHorizon(this Vector2Int instance)
        {
            instance.x = -instance.x;
            return instance;
        }
        public static Vector2Int FlipVertical(this Vector2Int instance)
        {
            instance.y = -instance.y;
            return instance;
        }

        public static Vector2Int ParseDirection(eDirection direction)
        {
            switch (direction)
            {
                case eDirection.Up:
                    return Vector2Int.up;
                case eDirection.Down:
                    return Vector2Int.down;
                case eDirection.Left:
                    return Vector2Int.left;
                case eDirection.Right:
                    return Vector2Int.right;
                default:
                case eDirection.None:
                    return Vector2Int.zero;
            }
        }

        public static bool BiggerThan(this Vector2Int instance, Vector2Int other)
        {
            return instance.x >= other.x && instance.y >= other.y && instance != other;
        }
    }
}