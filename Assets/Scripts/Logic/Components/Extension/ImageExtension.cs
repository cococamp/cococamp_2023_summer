using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public static class ImageExtension
    {
        public static void SetToSpriteCenter(this Image image, bool nativeSize = true)
        {
            if (image.sprite == null)
                return;

            if (nativeSize)
                image.SetNativeSize();

            var sprite = image.sprite;
            var pivot = sprite.pivot;
            var rt = image.rectTransform;

            var position = rt.localPosition;
            rt.pivot = new Vector2(pivot.x / sprite.rect.width, pivot.y / sprite.rect.height);
            rt.localPosition = position;
        }
    }
}