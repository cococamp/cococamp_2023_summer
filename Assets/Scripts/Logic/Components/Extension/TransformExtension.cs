using UnityEngine;
namespace Logic
{
    public static class TransformExtension
    {
        public static Transform GetChild(this Transform trans, string path)
        {
            return trans.Find(path);
        }

        public static T GetChild<T>(this Transform trans, string path)
        {
            return trans.GetChild(path).GetComponent<T>();
        }
    }
}