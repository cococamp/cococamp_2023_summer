using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Logic
{
    public static class RectIntExtension
    {
        public static RectInt Expand(this RectInt @this, Vector2Int value)
        {
            if (!@this.Contains(value))
            {
                if (value.x < @this.xMin) @this.xMin = value.x;
                if (value.x > @this.xMax) @this.xMax = value.x;
                if (value.y < @this.yMin) @this.yMin = value.y;
                if (value.y > @this.yMax) @this.yMax = value.y;
                return @this;
            }
            else
            {
                return @this;
            }
        }

        public static RectInt Zero => new RectInt(0, 0, 0, 0);

        public static IEnumerable<Vector2Int> Foreach(this RectInt @this)
        {
            for (int i = @this.xMin; i < @this.xMax; i++)
            {
                for (int j = @this.yMin; j < @this.yMax; j++)
                {
                    yield return new Vector2Int(i, j);
                }
            }
        }

        public static bool Equals(this RectInt @this, RectInt other)
        {
            return @this.position == other.position && @this.size == other.size;
        }

        public static Vector2Int RandomPos(this RectInt @this)
        {
            return StaticUtils.RandomPick(@this.Foreach());
        }
    }
}