using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Logic
{
    public static class ColorExtension
    {
        public static string ToRichTextString(this Color color)
        {
            StringExtension.stringToColorDict ??= new Dictionary<string, Color>();
            if (StringExtension.stringToColorDict.ContainsValue(color) == true)
            {
                foreach (var key in StringExtension.stringToColorDict.Keys)
                {
                    if (StringExtension.stringToColorDict[key] == color)
                    {
                        return key;
                    }
                }
            }
            var s = string.Format("{0:X2}{1:X2}{2:X2}", (int)(color.r * 255), (int)(color.g * 255), (int)(color.b * 255));
            StringExtension.stringToColorDict.SetSafely<string, Color>(s, color);
            return s;
        }
    }
}