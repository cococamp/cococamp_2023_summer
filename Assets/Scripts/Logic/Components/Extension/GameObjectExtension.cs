using UnityEngine;

namespace Logic
{
    public static class GameObjectExtension
    {
        public static void SetActiveOptimize(this GameObject go, bool isActive)
        {
            if (go.activeSelf != isActive)
            {
                go.SetActive(isActive);
            }
        }

        public static GameObject GetChild(this GameObject go, string path)
        {
            return go.transform.Find(path).gameObject;
        }

        public static T GetChild<T>(this GameObject go, string path)
        {
            return go.GetChild(path).GetComponent<T>();
        }
    }
}