using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static bool IsFilled(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        private static Color ToColor(this string text)
        {
            return ParseStringToColor(text);
        }

        public static Dictionary<string, Color> stringToColorDict;
        private static Color ParseStringToColor(string colorStr)
        {
            stringToColorDict ??= new Dictionary<string, Color>();
            if (stringToColorDict.TryGetValue(colorStr, out Color c)) return c;
            c = StringToColor(colorStr);
            stringToColorDict.Add(colorStr, c);
            return c;
        }
        private static Color StringToColor(string text)
        {
            Assert.IsNotNull(text, "A NULL text can't be convert to color");
            Assert.IsTrue(text.Length == 6, "The text can't be convert to color: " + text);

            int r = (HexToDecimal(text[0]) << 4) | HexToDecimal(text[1]);
            int g = (HexToDecimal(text[2]) << 4) | HexToDecimal(text[3]);
            int b = (HexToDecimal(text[4]) << 4) | HexToDecimal(text[5]);

            return new Color((float)r / 255f, (float)g / 255f, (float)b / 255f);
        }

        private static int HexToDecimal(char ch)
        {
            switch (ch)
            {
                case '0': return 0x0;
                case '1': return 0x1;
                case '2': return 0x2;
                case '3': return 0x3;
                case '4': return 0x4;
                case '5': return 0x5;
                case '6': return 0x6;
                case '7': return 0x7;
                case '8': return 0x8;
                case '9': return 0x9;
                case 'a':
                case 'A': return 0xA;
                case 'b':
                case 'B': return 0xB;
                case 'c':
                case 'C': return 0xC;
                case 'd':
                case 'D': return 0xD;
                case 'e':
                case 'E': return 0xE;
                case 'f':
                case 'F': return 0xF;
            }
            return 0xF;
        }

        public static string AddRichTextColor(this string s, Color color)
        {
            return $"<color=#{color.ToRichTextString()}>{s}</color>";
        }
    }
}