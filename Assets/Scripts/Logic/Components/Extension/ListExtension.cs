using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Logic
{
    public static class ListExtension
    {
        public static IEnumerable<T> RandomPick<T>(this List<T> @this, int pickCount)
        {
            return StaticUtils.RandomPick(@this, pickCount);
        }

        public static T RandomPick<T>(this List<T> @this)
        {
            return StaticUtils.RandomPick(@this);
        }
    }
}