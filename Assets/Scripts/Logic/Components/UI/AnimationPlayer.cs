using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace Logic
{
    /// <summary>
    /// 播放Image帧动画
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class AnimationPlayer : MonoBehaviour
    {
        public Sprite[] sprites;
        public int frameRate = 1;
        public bool loop = true;
        private float interval => 1.0f / frameRate;
        private Image image;
        private int currentIndex = 0;
        private float timeCounter = 0;
        void Awake()
        {
            image = GetComponent<Image>();
        }

        void Update()
        {
            if (sprites.Length <= 0) return;

            if (currentIndex >= sprites.Length) return;

            timeCounter += Time.deltaTime;
            if (timeCounter > interval)
            {
                timeCounter = 0;
                currentIndex++;
                if (currentIndex >= sprites.Length)
                {
                    if (loop)
                    {
                        currentIndex = 0;
                        image.sprite = sprites[currentIndex];
                    }
                }
                else
                {
                    image.sprite = sprites[currentIndex];
                }
            }
        }

        public void SetIndex(int index)
        {
            currentIndex = index;
        }
    }
}