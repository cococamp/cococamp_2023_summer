﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Logic;
using UnityEngine.InputSystem;


public class DrawLineBehaviour : MonoBehaviour
{
    public RawImage targetImage;
    public Action OnDrawBegin;
    public Action OnDrawDone;
    public Action OnUp;
    public Action OnDrag;
    public Func<bool> CanDraw;

    public float interval = 5f;

    public float lineWidth = 0.005f;

    bool beginDraw = false;

    Material lineMaterial;
    Vector3 curPos;
    List<Vector3> posList = new List<Vector3>();

    public List<Vector3> PosList => posList;

    private EventTriggerListener triggerListener;

    //public Camera rttCamera;
    RenderTexture rtt = null;

    bool animProcessing = false;

    public Texture brushTypeTexture;
    public Color brushColor = Color.black;

    public bool Redrawable { get; set; }

    private Vector3 startPosition = Vector3.zero;
    private Vector3 endPosition = Vector3.zero;
    public float BrashScale = 1.0f;
    private float brushScale = 0.2f;
    private float lastDistance;
    private Vector3[] controlPoints = new Vector3[4];
    private int b = 0;
    private float[] speedArray = new float[4];
    private int s = 0;

    private void Awake()
    {
        //targetImage.gameObject.SetActive(false);

        //rttCamera = GetComponent<Camera>();
        if (rtt == null)
        {
            var targetRectTrans = targetImage.transform as RectTransform;
            rtt = new RenderTexture((int)targetRectTrans.sizeDelta.x, (int)targetRectTrans.sizeDelta.y, 24);
            //rttCamera.targetTexture = rtt;
            targetImage.texture = rtt;
        }
        Clear(rtt);
        //rtt = rttCamera.targetTexture;

        lineMaterial = Resources.Load<Material>("Materials/LineMat");
        triggerListener = targetImage.gameObject.GetComponent<EventTriggerListener>();

        triggerListener.onDown += onDown;
        triggerListener.onDrag += onDrag;
        triggerListener.onUp += onUp;

        animProcessing = false;
    }

    public void Reset()
    {
        ClearLines();
        Clear(rtt);
    }

    private void OnEnable()
    {
        beginDraw = false;
        posList.Clear();
    }

    private void onDown(GameObject o)
    {
        if (!canDraw()) return;

        beginDraw = true;
        OnDrawBegin?.Invoke();
    }

    private void onDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (!canDraw()) return;

        if (!eventData.hovered.Contains(targetImage.gameObject))
        {
            return;
        }

        if (Vector3.Distance(curPos, Mouse.current.position.ReadValue()) > interval)
        {
            curPos = Mouse.current.position.ReadValue();

            posList.Add(new Vector3(curPos.x / Screen.width, curPos.y / Screen.height, 0));
        }

        var mousePos = Mouse.current.position.ReadValue();
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(targetImage.transform as RectTransform, mousePos, null, out var localPoint))
        {
            mousePos = localPoint;
            mousePos += (targetImage.transform as RectTransform).rect.size * 0.5f;
        }

        OnMouseMove(new Vector3(mousePos.x, mousePos.y, 0));
        targetImage.texture = rtt;

        OnDrag?.Invoke();
    }

    private void onUp(GameObject o)
    {
        //if (!gameObject.activeSelf)
        //{
        //    targetImage.gameObject.SetActive(false);
        //    return;
        //}

        if (!canDraw()) return;

        OnUp?.Invoke();

        if (posList.Count > 5 && OnDrawDone != null)
        {
            OnDrawDone();

            // 消失动画
            //rttCamera.enabled = false;
            //rttCamera.targetTexture = null;
            //Clear(rtt);
            // if (!Redrawable)
            //     targetImage.gameObject.SetActive(false);
        }

        beginDraw = false;
        ClearLines();
    }

    private bool canDraw()
    {
        if (CanDraw != null)
        {
            return CanDraw.Invoke();
        }
        return true;
    }

    private void OnMouseMove(Vector3 pos)
    {
        if (startPosition == Vector3.zero)
        {
            var mousePosition = Mouse.current.position.ReadValue();
            startPosition = new Vector3(mousePosition.x, mousePosition.y, 0);
        }

        endPosition = pos;
        float distance = Vector3.Distance(startPosition, endPosition);
        float resolutionScale = Mathf.Max(Screen.width, Screen.height) / 800f;
        distance /= resolutionScale;
        brushScale = SetScale(distance);
        ThreeOrderBézierCurse(rtt, pos, distance, 0.3f * 4.5f, 50);

        startPosition = endPosition;
        lastDistance = distance;
    }

    void ClearLines()
    {
        beginDraw = false;
        posList.Clear();
        curPos = Vector3.zero;

        startPosition = Vector3.zero;
        b = 0;
        s = 0;
    }

    void Clear(RenderTexture destTexture)
    {
        Graphics.SetRenderTarget(destTexture);
        GL.PushMatrix();
        GL.Clear(true, true, Color.clear);
        GL.PopMatrix();
    }

    void DrawLine(List<Vector3> vectices)
    {
        //if (!beginDraw)
        //    return;

        //GL.PushMatrix();
        //GL.LoadOrtho();

        //lineMaterial.SetPass(0);

        ////GL.Begin(GL.LINES);
        ////for (int i = 0; i < vectices.Count - 1; ++i)
        ////{
        ////    GL.Vertex3(vectices[i].x, vectices[i].y, vectices[i].z);
        ////    GL.Vertex3(vectices[i + 1].x, vectices[i + 1].y, vectices[i + 1].z);
        ////}
        ////GL.End();

        //float width = lineWidth;
        //Vector3 a, b, c, d, w;
        //Vector3 sub;
        //GL.Begin(GL.QUADS);

        //for (int i = 0; i < vectices.Count - 1; ++i)
        //{
        //    a = vectices[i];
        //    b = vectices[i + 1];
        //    sub = (b - a).normalized;

        //    a = a - width * sub;
        //    b = b + width * sub;

        //    w = (Vector3.Cross(Vector3.forward, b - a)).normalized;
        //    c = a + w * width;
        //    d = b + w * width;
        //    a = a - w * width;
        //    b = b - w * width;

        //    GL.Vertex3(a.x, a.y, a.z);
        //    GL.Vertex3(c.x, c.y, c.z);
        //    GL.Vertex3(d.x, d.y, d.z);
        //    GL.Vertex3(b.x, b.y, b.z);
        //}

        //GL.End();

        //GL.PopMatrix();
    }

    private void OnRenderObject()
    {
        //DrawLine(posList);
    }

    //三阶贝塞尔曲线，获取连续4个点坐标，通过调整中间2点坐标，画出部分（我使用了num/1.5实现画出部分曲线）来使曲线平滑;通过速度控制曲线宽度。
    private void ThreeOrderBézierCurse(RenderTexture renderTexture, Vector3 pos, float distance, float targetPosOffset, int num)
    {
        //记录坐标
        controlPoints[b] = pos;
        b++;
        //记录速度
        speedArray[s] = distance;
        s++;
        if (b == 4)
        {
            Vector3 temp1 = controlPoints[1];
            Vector3 temp2 = controlPoints[2];

            //修改中间两点坐标
            Vector3 middle = (controlPoints[0] + controlPoints[2]) / 2;
            controlPoints[1] = (controlPoints[1] - middle) * 1.5f + middle;
            middle = (temp1 + controlPoints[3]) / 2;
            controlPoints[2] = (controlPoints[2] - middle) * 2.1f + middle;

            for (int index = 0; index < num / 1.5f; index++)
            {
                float u = (1.0f * index) / num;
                Vector3 target = controlPoints[0] * Mathf.Pow(1 - u, 3) +
                                 3 * controlPoints[1] * u * Mathf.Pow(1 - u, 2) +
                                 3 * controlPoints[2] * u * u * (1 - u) +
                                 controlPoints[3] * Mathf.Pow(u, 3);

                //获取速度差值
                float deltaspeed = (float)(speedArray[3] - speedArray[0]) / num;
                //模拟毛刺效果
                var randomOffset = Random.insideUnitCircle;
                DrawBrush(renderTexture, (int)(target.x + randomOffset.x), (int)(target.y + randomOffset.y), brushTypeTexture, brushColor, SetScale(speedArray[0] + (deltaspeed * index)));
            }

            controlPoints[0] = temp1;
            controlPoints[1] = temp2;
            controlPoints[2] = controlPoints[3];

            speedArray[0] = speedArray[1];
            speedArray[1] = speedArray[2];
            speedArray[2] = speedArray[3];
            b = 3;
            s = 3;
        }
        else
        {
            DrawBrush(renderTexture, (int)endPosition.x, (int)endPosition.y, brushTypeTexture,
                brushColor, brushScale);
        }
    }

    //设置画笔宽度
    private float SetScale(float distance)
    {
        float Scale = 0;
        if (distance < 100)
        {
            Scale = 0.8f - 0.005f * distance;
        }
        else
        {
            Scale = 0.425f - 0.00125f * distance;
        }
        if (Scale <= 0.05f)
        {
            Scale = 0.05f;
        }
        Scale *= 0.16f;
        Scale *= BrashScale;
        return Scale;
    }

    private void DrawBrush(RenderTexture destTexture, int x, int y, Texture sourceTexture, Color color, float scale)
    {
        float resolutionScale = Mathf.Max(Screen.width, Screen.height) / 800f;
        int texWidth = (int)(sourceTexture.width * resolutionScale);
        int texHeight = (int)(sourceTexture.width * resolutionScale);

        DrawBrush(destTexture, new Rect(x, y, texWidth, texHeight), sourceTexture, color, scale);
    }

    private void DrawBrush(RenderTexture destTexture, Rect destRect, Texture sourceTexture, Color color, float scale)
    {
        float left = destRect.xMin - destRect.width * scale / 2.0f;
        float right = destRect.xMin + destRect.width * scale / 2.0f;
        float top = destRect.yMin - destRect.height * scale / 2.0f;
        float bottom = destRect.yMin + destRect.height * scale / 2.0f;

        Graphics.SetRenderTarget(destTexture);

        GL.PushMatrix();
        GL.LoadOrtho();

        lineMaterial.SetTexture("_MainTex", brushTypeTexture);
        lineMaterial.SetColor("_Color", color);
        lineMaterial.SetPass(0);

        GL.Begin(GL.QUADS);

        var targetImageRect = (targetImage.transform as RectTransform).rect;
        GL.TexCoord2(0.0f, 0.0f); GL.Vertex3(left / targetImageRect.width, top / targetImageRect.height, 0);
        GL.TexCoord2(1.0f, 0.0f); GL.Vertex3(right / targetImageRect.width, top / targetImageRect.height, 0);
        GL.TexCoord2(1.0f, 1.0f); GL.Vertex3(right / targetImageRect.width, bottom / targetImageRect.height, 0);
        GL.TexCoord2(0.0f, 1.0f); GL.Vertex3(left / targetImageRect.width, bottom / targetImageRect.height, 0);

        // GL.TexCoord2(0.0f, 0.0f); GL.Vertex3(left / Screen.width, top / Screen.height, 0);
        // GL.TexCoord2(1.0f, 0.0f); GL.Vertex3(right / Screen.width, top / Screen.height, 0);
        // GL.TexCoord2(1.0f, 1.0f); GL.Vertex3(right / Screen.width, bottom / Screen.height, 0);
        // GL.TexCoord2(0.0f, 1.0f); GL.Vertex3(left / Screen.width, bottom / Screen.height, 0);

        // GL.TexCoord2(0.0f, 0.0f); GL.Vertex3(0, 0, 0);
        // GL.TexCoord2(1.0f, 0.0f); GL.Vertex3(1, 0, 0);
        // GL.TexCoord2(1.0f, 1.0f); GL.Vertex3(1, 1, 0);
        // GL.TexCoord2(0.0f, 1.0f); GL.Vertex3(0, 1, 0);

        GL.End();
        GL.PopMatrix();
    }
}
