namespace Logic
{
    public enum GameOperatorState
    {
        Intro,
        Level,
    }
    public class GameOperatorManager : BaseOperateMachine<GameOperatorState>
    {
        protected override GameOperatorState initState => GameOperatorState.Intro;

        protected override void RegisterAllOperator()
        {
            RegisterOperator(GameOperatorState.Intro, new GameOperator_Intro(this));
            RegisterOperator(GameOperatorState.Level, new GameOperator_Level(this));
        }
    }
}