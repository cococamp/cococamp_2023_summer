namespace Logic
{
    public class GameOperator_Intro : BaseOperator<GameOperatorState>
    {
        public GameOperator_Intro(BaseOperateMachine<GameOperatorState> machine) : base(machine)
        {
        }

        public override GameOperatorState state => GameOperatorState.Intro;

        public override void Enter()
        {
            SceneController.instance.LoadEmpty(null);
            GameController.instance.ShowUI(UISort.IntroUI);
            AudioManager.instance.PlayBGM("BGM");
        }

        public override void Exit()
        {
            GameController.instance.DestroyUI(UISort.IntroUI);
        }

        public override void Update()
        {

        }
    }
}