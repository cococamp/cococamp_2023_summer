using UnityEngine.SceneManagement;

namespace Logic
{
    public class GameOperatorParam_Level
    {
        public string levelID;
    }
    public class GameOperator_Level : BaseOperator<GameOperatorState, GameOperatorParam_Level>
    {
        public GameOperator_Level(BaseOperateMachine<GameOperatorState> machine) : base(machine)
        {
        }

        public override GameOperatorState state => GameOperatorState.Level;

        public override bool CanEnter(GameOperatorParam_Level param)
        {
            return !SceneController.instance.IsLoading;
        }

        public override void Enter(GameOperatorParam_Level param)
        {
            StartGame();

            SceneController.instance.LoadScene(param.levelID, (success) =>
            {
                if (success)
                {

                }
                else
                {
                    GameController.instance.LogError("加载Level失败  请检查");
                }
            });
        }

        private void StartGame()
        {
            OrganController.instance.Model.InitData();
            PillController.instance.Model.InitData();
            ExpController.instance.Model.InitData();
            GameController.instance.ShowUI(UISort.PrescriptionUI);
        }

        public override void Exit()
        {
            GameController.instance.HideUI(UISort.PrescriptionUI);
            GameController.instance.HideUI(UISort.GameFinishUI);
            TipsController.instance.DestroyAllMsgBoxUI();
            PillController.instance.DestroyAllEntities();
        }

        public override void Update()
        {

        }
    }
}