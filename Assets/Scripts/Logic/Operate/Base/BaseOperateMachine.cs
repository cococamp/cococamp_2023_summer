using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Logic
{
    public abstract class BaseOperateMachine<T>
    {
        public BaseOperator<T> currentOperator;
        private Dictionary<T, BaseOperator<T>> operators = new Dictionary<T, BaseOperator<T>>();
        public T currentState;

        protected abstract T initState { get; }
        
        public void Init()
        {
            operators = new Dictionary<T, BaseOperator<T>>();
            RegisterAllOperator();
            ChangeState(initState);
        }

        /// <summary>
        /// 注册所有的Operator
        /// </summary>
        protected abstract void RegisterAllOperator();

        protected void RegisterOperator(T state, BaseOperator<T> op)
        {
            operators.Add(state, op);
        }

        public void Update()
        {
            if (currentOperator != null)
            {
                currentOperator.Update();
            }
        }

        public virtual bool ChangeState<U>(T state, U param)
        {
            if (currentOperator != null && !currentOperator.CanBreak(state))
            {
                return false;
            }
            BaseOperator<T> newOperator = null;
            if (!operators.TryGetValue(state, out newOperator))
            {
                Debug.LogError($"OperateMachine<{typeof(T).Name}>.ChangeState<{typeof(U).Name}>({state}, {param})未添加Operator类型<{state}>");
                return false;
            }

            var changedType = newOperator as BaseOperator<T, U>;
            if (changedType == null)
            {
                Debug.LogError($"OperateMachine<{typeof(T).Name}>.ChangeState<{typeof(U).Name}>({state}, {param})的参数类型不正确");
                return false;
            }

            if (!changedType.CanEnter(param))
            {
                return false;
            }

            if (currentOperator != null)
            {
                currentOperator.Exit();
            }

            currentState = state;
            currentOperator = newOperator;
            changedType.Enter(param);
            return true;
        }

        public virtual bool ChangeState(T state)
        {
            if (currentOperator != null && !currentOperator.CanBreak(state))
            {
                return false;
            }

            if (!operators.TryGetValue(state, out BaseOperator<T> newOperator))
            {
                Debug.LogError($"OperateMachine<{typeof(T).Name}>.ChangeState({state})未添加Operator类型<{state}> ");
                return false;
            }

            if (!newOperator.CanEnter())
            {
                return false;
            }

            if (currentOperator != null)
            {
                currentOperator.Exit();
            }

            currentState = state;
            currentOperator = newOperator;
            currentOperator.Enter();
            return true;
        }

        public void Destroy()
        {
            if (currentOperator != null)
            {
                currentOperator.Exit();
                currentOperator = null;
            }

            foreach (var ope in operators.Values)
            {
                ope.Destroy();
            }
            operators = null;
        }
    }
}