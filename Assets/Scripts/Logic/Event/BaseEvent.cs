using System;

namespace Logic
{
    public class BaseEvent
    {
        public BaseEvent()
        {

        }
    }
    public class Event : BaseEvent
    {
        private event Action listener = delegate { };
        public void AddListener(Action callback)
        {
            listener += callback;
        }

        public void RemoveListener(Action callback)
        {
            listener -= callback;
        }

        public void Fire()
        {
            if (listener != null) listener.Invoke();
        }
    }

    public class Event<T> : BaseEvent
    {
        private event Action<T> listener = delegate { };
        public void AddListener(Action<T> callback)
        {
            listener += callback;
        }

        public void RemoveListener(Action<T> callback)
        {
            listener -= callback;
        }

        public void Fire(T param)
        {
            listener.Invoke(param);
        }
    }

    public class Event<T, U> : BaseEvent
    {
        private event Action<T, U> listener = delegate { };
        public void AddListener(Action<T, U> callback)
        {
            listener += callback;
        }

        public void RemoveListener(Action<T, U> callback)
        {
            listener -= callback;
        }

        public void Fire(T param1, U param2)
        {
            listener.Invoke(param1, param2);
        }
    }
}