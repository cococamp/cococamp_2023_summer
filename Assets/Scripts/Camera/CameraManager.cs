using UnityEngine;
using System.Collections;
using System;
using Cinemachine.PostFX;
using UnityEngine.Rendering.PostProcessing;

namespace Cameras
{
    public class CameraManager : SingletonMonoBehaviourClass<CameraManager>
    {
        public SceneCamera sceneCamera;
        public PostProcessProfile postProcessProfile;
        public UICamera uiCamera;
    }
}