using UnityEngine;

namespace Cameras
{
    public class UICamera : SingletonMonoBehaviourClass<UICamera>
    {
        public Camera cameraComponent;
        protected override void onAwake()
        {
            cameraComponent = transform.GetComponent<Camera>();
        }
    }
}