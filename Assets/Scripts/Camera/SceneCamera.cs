using UnityEngine;
using Cinemachine;

namespace Cameras
{
    public class SceneCamera : MonoBehaviour
    {
        public Camera cameraComponent;
        public CinemachineBrain brain;
        void Awake()
        {
            cameraComponent = transform.GetComponent<Camera>();
        }

        public void SetTarget(Transform target)
        {
            brain.ActiveVirtualCamera.LookAt = target;
            brain.ActiveVirtualCamera.Follow = target;
        }

        public void SetConfiner(Collider2D collider2D)
        {
            brain.ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CinemachineConfiner>().m_BoundingShape2D = collider2D;
        }

        public void SetSide(bool left)
        {
            if(left)
            {
                cameraComponent.rect = new Rect(0, 0, 0.5f, 1);
            }
            else
            {
                cameraComponent.rect = new Rect(0.5f, 0, 0.5f, 1);
            }
        }

        public void SetNormal()
        {
            cameraComponent.rect = new Rect(0, 0, 1, 1);
        }
    }
}